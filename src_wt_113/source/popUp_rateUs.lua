
-- -----------------------------------------------------------------------------------
-- Main Menu.lua
-- -----------------------------------------------------------------------------------

local composer = require( "composer" )
local globals = require( "globals" )
local widget = require( "widget" )
local inspect = require( "inspect" )
local displayGroups = {}

local scene = composer.newScene()



-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

local function handleReleaseEvent( event )
	local buttonID = event.target.id
	
	if buttonID == "yes" then
		local options =
		{
			iOSAppId = "1276455019",
			supportedAndroidStores = { "google", "samsung", "amazon" },
		}
		native.showPopup( "rateApp", options )
		composer.hideOverlay("fade", 500 )
		
		_G.rateDone = true
		settings.set("rateDone", _G.rateDone)
		
	elseif buttonID == "no" then
		composer.hideOverlay("fade", 500 )
	end
	
	_G.doRateUs = false
	settings.set('doRateUs', _G.doRateUs)

    _G.playerRunCount = 0
    settings.set('runCount', _G.playerRunCount)
end

-- create()
function scene:create( event )

    local sceneGroup = self.view
	font = "res/fonts/BalooTammudu-Regular.ttf"
	
    local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
    local mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
	
	local rectOverlay = display.newRect(display.contentCenterX, display.screenOriginY ,display.actualContentWidth + 20, 3000)
	rectOverlay.anchorX = 0.5
	rectOverlay:setFillColor(0.08,0,0, 0.7)
	backGroup:insert(rectOverlay)	
		
	local messageBox = newImageRectNoDimensions("res/graphics/game/dialogWin.png")
	messageBox.x = display.contentCenterX
	messageBox.y = 150
	mainGroup:insert(messageBox)
	
	local titleText = display.newText("RATE WORD TREATS!",  display.contentCenterX, messageBox.y - 50, font, 20)
	titleText:setFillColor(0.8,0,0)
	mainGroup:insert(titleText)
	
	local options = {
		parent = mainGroup,
		text = "Help us improve Word Treats by providing your feedback.",
		x = display.contentCenterX,
		y = messageBox.y + 40,
		width = messageBox.width - 60,
		font = font,
		fontSize = 16,
		align = "center"
	}
	
	local descText = display.newText(options)
	descText:setFillColor(0,0,0)
	
	local yesBtn = globals.createImageButton( "yes",
				display.contentCenterX - 55, descText.y + descText.height - 15,
				"res/graphics/game/btn_YesUp.png", "res/graphics/game/btn_YesDown.png",
				mainGroup, handleReleaseEvent, 91, 36)
	
	local noBtn = globals.createImageButton( "no",
				display.contentCenterX + 55, descText.y + descText.height - 15,
				"res/graphics/game/btn_NoUp.png", "res/graphics/game/btn_NoDown.png",
				mainGroup, handleReleaseEvent, 91, 36)
	
	mainGroup.y = - 1000
	transition.to(mainGroup, {delay = 400, time = 1400, y = display.screenOriginY + 55, transition = easing.inOutElastic})

end
-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
		parent = event.parent
    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
			composer.removeScene( "popUp_rateUs" )
    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene