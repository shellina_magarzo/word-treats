
-- -----------------------------------------------------------------------------------
-- Main Menu.lua
-- -----------------------------------------------------------------------------------

local composer = require( "composer" )
local globals = require( "globals" )
local widget = require( "widget" )
local inspect = require( "inspect" )
local displayGroups = {}

local scene = composer.newScene()

local top = display.screenOriginY
local left = display.screenOriginX
local bottom = display.contentHeight - display.screenOriginY
local right = display.contentWidth - display.screenOriginX


local font_Header = "res/fonts/BalooTammudu-Regular.ttf"
local font_Sub = "res/fonts/Sriracha-Regular.ttf"
local font_Num = "res/fonts/DUNKIN_SANS.otf"

-- If the phone is an Iphone X then 
if onIphoneX then
	top = display.screenOriginY + 25	
end 

-- the group that will spin
local rouletteGroup
local respinPopupGroup

-- Roulette wheel image
local roulette

-- The arrow that points to the winning slice
local pointer

-- Is the roulette wheel currently spinning?
local isSpinning = false

_G.rewardVideoComplete = false

local goBtn
local coinsBtn
local coinsTxt
local descText2

-- Starts from the right of pointer going clockwise
local coinPrices_Spin1 = {
	10, 50, 25, 50, 25, 10, 10, 25
}
local coinPrices_Spin2 = {
	0, 50, 25, 50, 25, 10, 10, 25
}
local coinPrices_Current = { } -- The one for the current spin

-- A table of the prices text objects, used on the first and second roulette
local pricesText =  {} 

-- Produces a different sequence each time (assuming enough time between invocations)
math.randomseed( os.time() )

_G.loadSoundTable()

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------

local function displayRewardAd( )
	if ( _G.admob.isLoaded( "rewardedVideo" )) then
		_G.admob.show( "rewardedVideo" )
		print ("REWARDED VIDEO: Showing Reward Ad [Roulette Wheel]")
	end
end

-- Called to set the daily reward bool to false and store the date
function dailyRewardDone( )
	showDailyReward = false
	settings.set('daily reward', showDailyReward)

	local date = os.date("*t")
	local datetoday = tostring(date.year.."-"..string.format("%02d", date.month).."-"..string.format("%02d", date.day))
	local pattern = "(%d+)%-(%d+)%-(%d+)"
	local xyear, xmonth, xday = datetoday:match(pattern)
	
	settings.set('day_stored_reward', xday)
	settings.set('month_stored_reward', xmonth)
end

-- Updates the coin text and saves it
function updateCoins( result )
	_G.coinVal = _G.coinVal + result 
	coinTxt.text = _G.coinVal
	settings.set('coins', _G.coinVal)
end	

-- Creates the coin effect animation (only one coin)
local function coinEmitterEffect(  )
	local effectIcon = newImageRectNoDimensions( "res/graphics/game/coinBig.png" )
	effectIcon.anchorX = 1
	effectIcon:scale(1,1)
	effectIcon.x = goBtn.x
	effectIcon.y = goBtn.y
	effectIcon.alpha = 1
	transition.to (effectIcon, {time = 300, x = coinsBtn.x, y = coinsBtn.y, alpha = 0.5, yScale = 0.5, xScale = 0.5, onComplete = function() 
						display.remove(effectIcon) effectIcon = nil 
						end })
end

local function coinCountUpEffect( )
	updateCoins( 5 )
end

local function hideSpin2Popup(  )
	transition.to(respinPopupGroup, {time = 500, y = -1000, transition = easing.inOutExpo})
end

local function showSpin2Popup(  )
	if coinPrices_Current == coinPrices_Spin1 then
		if _G.admob.isLoaded( "rewardedVideo" ) then
			descText2.text = "Watch a video"
		end
		respinPopupGroup.isVisible = true
		respinPopupGroup.y = -1000
		transition.to(respinPopupGroup, {time = 500, y = -15, transition = easing.inOutExpo})
	else
		hideSpin2Popup()
		composer.hideOverlay("slideUp", 500 )
	end
end

local function shakePointer( )
	transition.to( pointer, {time = 100, rotation = -20, 
								onComplete = function()
									transition.to (pointer, {time = 100, rotation = 0} )
								end} )	
end

-- Custom easing function
-- source: http://davebollinger.org/corona-quick-thought-a-custom-inoutback-easing-function/
-- other source: https://github.com/EmmanuelOga/easing/blob/master/lib/easing.lua
-- t, d , b , c
local function inOutCubicOvershoot(t,tmax,start,delta) -- mimics "inOutBack"
	local s = 1.0 -- overshoot factor for in, alter to suit
	local s2 = 0.5 -- overshoot factor for out
	local icurve = function(t) return (s+1)*t*t*t-s*t*t end
	local icurve2 = function(t) return (s2+1)*t*t*t-s2*t*t end

	local ocurve = function(t) return 1-icurve2(1-t) end
	local iocurve = function(t) return (t<0.5) and icurve(t*2)/2 or ocurve(t*2-1)/2+0.5 end
	return start + iocurve(t/tmax) * delta
end

-- Spins the roulette and generate the random prize
local function spinRoulette(  )
	local randomRot = math.random( 360 )

	-- Prevents the pointer from landing on an edge
	while (randomRot % 45) == 0 do
		randomRot = randomRot + ( math.random( -10, 10 ) )
	end

	-- The coin price that the player has won
	local result = coinPrices_Current[ math.ceil( (randomRot / 45) ) ]

	-- Animates the spin. Spins 4 times first before ending on the final slice.
	transition.to(rouletteGroup, {delay = 300, time = 5000, rotation =  2160 - randomRot, transition = inOutCubicOvershoot,
									onComplete = function()										
												-- Calls the animation for the coin effect and the coin text animation
												if result ~= 0 then
													timer.performWithDelay( 100, coinEmitterEffect, result/5 )
													transition.to ( coinTxt, {time = 700, size = 20, 
																	onComplete = function() 
																	transition.to( coinTxt, { time = 100, size = 15}) end } )
													timer.performWithDelay( 100, coinCountUpEffect, result/5 )
													audio.play(_G.soundTable["levelEnd"],{ channel=audio.findFreeChannel() })
													
													timer.performWithDelay( 300, function()
																					audio.play(_G.soundTable["claimGold"],{ channel=audio.findFreeChannel() })
																				end )
												else
													audio.play(_G.soundTable["lose"],{ channel=audio.findFreeChannel() })
												end

												timer.performWithDelay(1500, showSpin2Popup)
												isSpinning = false
									end })
	timer.performWithDelay( 600, function ()
									transition.to( pointer, {time = 100, rotation = 20})
									end )

	timer.performWithDelay( 1500, function ()
									isSpinning = true
									end )
	timer.performWithDelay( 4500, function ()
									isSpinning = false
									end )
	print( "Coin Reward: " .. result )
end

-- Update function
local frameCount = 0

-- For the tick sound during the spin
function update(event)
    if rouletteGroup and isSpinning then
    	local rot = math.ceil(rouletteGroup.rotation)

    	if ( (frameCount % 6) == 0 and rot ~= 0) then
    		audio.play(_G.soundTable["tick"],{ channel=5 })
    		shakePointer()
    	end
    end

    frameCount = frameCount + 1
end
 
Runtime:addEventListener("enterFrame", update) 

-- Updates the text depending on the current spin round
local function updateCoinPrices(  )
	for i = #pricesText, 1, -1 do
		display.remove( pricesText[i] )
    	table.remove(pricesText,i)
	end
	-- The text for the prices in the roulette wheel
	for i = 1, #coinPrices_Current do
		local rotX, rotY, rot

		if ( i == 1 ) then
			rotX = display.contentCenterX + 20
			rotY = display.contentCenterY - 50
			rot = 20
		elseif ( i == 2 ) then
			rotX = display.contentCenterX + 50
			rotY = display.contentCenterY - 20
			rot = 70
		elseif ( i == 3 ) then
			rotX = display.contentCenterX + 48
			rotY = display.contentCenterY + 18
			rot = 120
		elseif ( i == 4 ) then
			rotX = display.contentCenterX + 18
			rotY = display.contentCenterY + 48
			rot = 160
		elseif ( i == 5 ) then
			rotX = display.contentCenterX - 18
			rotY = display.contentCenterY + 48
			rot = -160
		elseif ( i == 6 ) then
			rotX = display.contentCenterX - 48
			rotY = display.contentCenterY + 18
			rot = -120
		elseif ( i == 7 ) then
			rotX = display.contentCenterX - 50 
			rotY = display.contentCenterY - 20
			rot = -70
		elseif ( i == 8 ) then
			rotX = display.contentCenterX - 20
			rotY = display.contentCenterY - 50
			rot = -20
		end

		local textOptions = {
			parent = rouletteGroup,
			text = coinPrices_Current[i],
			x = rotX,
			y = rotY,
			font = font_Sub,
			fontSize = 20,
			align = "center"
		}

		local text = display.newText( textOptions )
		text:setFillColor( 0.5607, 0.1764, 0.0078 )
		if (coinPrices_Current[i] == 0) then text.alpha = 0 end
		text.rotation = rot
		table.insert( pricesText, text )
	end
end

local function showSpin2( )
	-- The actual roulette wheel (spin 2)
	rouletteGroup:remove( roulette )
	roulette = newImageRectNoDimensions("res/graphics/menu/roulette_spin2.png")
	roulette.x = display.contentCenterX
    roulette.y = display.contentCenterY
	rouletteGroup:insert(roulette)
	roulette.alpha = 0

	for i = 1, #pricesText do
		pricesText[i].alpha = 0
		transition.to(pricesText[i], {delay = 100, time = 500, alpha =  1, transition = easing.linear})
	end
	goBtn.alpha = 0

	transition.to(roulette, {delay = 100, time = 500, alpha =  1, transition = easing.linear})
	transition.to(goBtn, {delay = 100, time = 500, alpha =  1, transition = easing.linear})

	rouletteGroup.rotation = 0
	goBtn:setEnabled( true )
	coinPrices_Current = coinPrices_Spin2
	updateCoinPrices()
end

local function handleReleaseEvent( event )
	local buttonID = event.target.id

	_G.audioButtonSelect()
	
	if buttonID == "go" then
		spinRoulette()
		goBtn:setEnabled( false )
		dailyRewardDone( )
	elseif buttonID == "respin_no" then
		composer.hideOverlay("slideUp", 500 )
	elseif buttonID == "respin_yes" then
		if _G.admob.isLoaded( "rewardedVideo" ) then
			displayRewardAd()
			hideSpin2Popup()

			timer.performWithDelay( 500, function ( )
				if _G.rewardVideoComplete then
					showSpin2()
				else
					composer.hideOverlay("slideUp", 500 )
				end
			end)
		else
			hideSpin2Popup()
			showSpin2()
			updateCoins( -10 )
		end
	end

end

-- create()
function scene:create( event )

    local sceneGroup = self.view
	
	-- Display Groups
    local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
    local mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
    rouletteGroup = globals.createDisplayGroup( "rouletteGroup", displayGroups, sceneGroup )
    local frontGroup = globals.createDisplayGroup( "frontGroup", displayGroups, sceneGroup )
    respinPopupGroup = globals.createDisplayGroup( "respinPopupGroup", displayGroups, sceneGroup )

    -- Background black overlay
	local rectOverlay = display.newRect(display.contentCenterX, display.screenOriginY ,display.actualContentWidth + 20, 3000)
	rectOverlay.anchorX = 0.5
	rectOverlay:setFillColor(0.08,0,0, 0.7)
	backGroup:insert(rectOverlay)

	-- Coins button and coins text
	coinsBtn = globals.createImageButton( "coins", 
                        0, top + 30,
                        "res/graphics/game/btnCoinsUp.png", "res/graphics/game/btnCoinsDown.png",
                        mainGroup, handleReleaseEvent, 110, 32)
    _G.anchor.Center( coinsBtn )
	coinsBtn.x = (display.contentWidth - display.screenOriginX) - ( coinsBtn.contentWidth * 0.5 ) - 2
	coinsBtn:setEnabled( false )

	-- "DAILY REWARD" text
	local titleImg = newImageRectNoDimensions("res/graphics/menu/rewardText.png")
	globals.setBetterAnchor( titleImg )
	titleImg.x = display.contentCenterX - titleImg.width/2
	titleImg.y = coinsBtn.y + 20
	mainGroup:insert(titleImg)

	local textSettings =
	{
		text = _G.coinVal,
		x = coinsBtn.x - 10,
		y = coinsBtn.y + 2,
		width = 45,
		height = 30,
		font = font_Header,
		fontSize = 15,
		align = "right"
	}
	
	coinTxt = display.newText (textSettings)
	coinTxt:setFillColor(1,1,1)
	mainGroup:insert(coinTxt)

	-- The actual roulette wheel (spin 1)
	roulette = newImageRectNoDimensions("res/graphics/menu/roulette.png")
	roulette.x = display.contentCenterX
    roulette.y = display.contentCenterY
	rouletteGroup:insert(roulette)

	-- The arrow that points to the winning slice
	pointer = newImageRectNoDimensions("res/graphics/menu/wheelPointer.png")
	pointer.x = roulette.x
	pointer.y = titleImg.y + 135
	frontGroup:insert( pointer )

	-- The "GO" button in the middle of the roulette wheel
	goBtn = globals.createImageButton( "go", 
					display.contentCenterX,
					titleImg.y + 270,
					"res/graphics/menu/btnPlayWheelUp.png",
					"res/graphics/menu/btnPlayWheelDown.png",
					frontGroup,
					handleReleaseEvent,
					62, 62	)

	coinPrices_Current = coinPrices_Spin1
	updateCoinPrices()

	-- Creates the spin 2 prompt popup but hides it for now
	respinPopupGroup.isVisible = false
				
	local bgRespin = newImageRectNoDimensions("res/graphics/game/popUp.png")
	bgRespin:scale (0.7,0.7)
	bgRespin.x = display.contentCenterX
	bgRespin.y = 150
	respinPopupGroup:insert(bgRespin)

	local options1 = {
		parent = respinPopupGroup,
		text = "Would you like to",
		x = display.contentCenterX,
		y = bgRespin.y - 50,
		width = bgRespin.width - 90,
		font = font_Header,
		fontSize = 18,
		align = "center"
	}
	local options1_2 = {
		parent = respinPopupGroup,
		text = "spin again?",
		x = display.contentCenterX,
		y = bgRespin.y - 25,
		width = bgRespin.width - 90,
		font = font_Header,
		fontSize = 18,
		align = "center"
	}
	local options2 = {
		parent = respinPopupGroup,
		text = "Costs 10 coins",
		x = display.contentCenterX,
		y = bgRespin.y,
		width = bgRespin.width - 90,
		font = font_Sub,
		fontSize = 15,
		align = "center"
	}
	
	local descText1 = display.newText(options1)
	descText1:setFillColor(0.5607, 0.1764, 0.0078)
	local descText1_2 = display.newText(options1_2)
	descText1_2:setFillColor(0.5607, 0.1764, 0.0078)
	descText2 = display.newText(options2)
	descText2:setFillColor(0.5607, 0.1764, 0.0078)
	
	local yesBtn = globals.createImageButton( "respin_yes",
				display.contentCenterX - 45, descText2.y + descText2.height + 25 ,
				"res/graphics/game/btn_YesUp.png", "res/graphics/game/btn_YesDown.png",
				respinPopupGroup, handleReleaseEvent, 91, 36)
	
	local noBtn = globals.createImageButton( "respin_no",
				display.contentCenterX + 45, descText2.y + descText2.height + 25,
				"res/graphics/game/btn_NoUp.png", "res/graphics/game/btn_NoDown.png",
				respinPopupGroup, handleReleaseEvent, 91, 36)

	yesBtn:scale (0.9,0.9)
	noBtn:scale (0.9,0.9)

	-- So that all the children rotate on the same axis
	rouletteGroup.anchorChildren = true
    rouletteGroup.x = display.contentCenterX


    -- Animation for when the popup enters the screen
	mainGroup.y = - 1000
	rouletteGroup.y = - 1000
	frontGroup.y = - 1000

	transition.to(mainGroup, {delay = 400, time = 1400, y = 0, transition = easing.inOutElastic})
	transition.to(rouletteGroup, {delay = 400, time = 1400, y = titleImg.y + 270, transition = easing.inOutElastic})
	transition.to(frontGroup, {delay = 400, time = 1400, y = 0, transition = easing.inOutElastic})
end
-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
		--parent = event.parent
    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
        Runtime:removeEventListener( "enterFrame", update )

    elseif ( phase == "did" ) then
    	for s,v in pairs( _G.soundTable ) do
			audio.dispose( _G.soundTable[s] )
			_G.soundTable[s] = nil
		end
        -- Code here runs immediately after the scene goes entirely off screen
			composer.removeScene( "popUp_wheel" )
    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene