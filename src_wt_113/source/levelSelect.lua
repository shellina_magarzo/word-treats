
-- -----------------------------------------------------------------------------------
-- Level Select.lua
-- -----------------------------------------------------------------------------------

local composer = require( "composer" )	
local globals = require( "globals" )
local widget = require( "widget" )
local inspect = require( "inspect" )

local scene = composer.newScene()
local displayGroups = {}
local levels = {}

_G.lastScene = "levelSelect"

local GRAY = { r = 0.7019607, g = 0.8, b = 0.7019607 }
local BLACK = { r = 0, g = 0.5, b = 0 }

local menuName = composer.getVariable("menuName") -- Soup Name 
local levelName = composer.getVariable("levelName") -- Ingredient Soup Name
local treatIngName = composer.getVariable("treatIngName")  -- Ingredient Treat Name 
local levelCount = composer.getVariable("levelCount")
local dataIndex = composer.getVariable("dataIndex")

print("LEVEL SELECT: " .. levelName .. " has " .. levelCount .. " levels.")

local cX = display.contentCenterX
local top = display.screenOriginY
local left = display.screenOriginX
local bottom = display.contentHeight - display.screenOriginY
local right = display.contentWidth - display.screenOriginX

local data = 1
local levelTable = {}

for i=1, levelCount+3 do
	levelTable[i] = {}
end

levelTable[1].type = 1
levelTable[2].type = 2

for i=1, levelCount do
	levelTable[i+2].type = 3
end

levelTable[levelCount+3].type = 4

local coinTxt = nil
local timerEmphasis

local menuCardOptions =
{
	frames = 
	{ 
		{	-- frame 1 / crownFrame
			x = 0,
			y = - 0,
			width = 304,
			height = 22
		},
		{	-- frame 2 / topFrame (title i.e. Prep Cook/Apprentice)
			x = 0,
			y = 22,
			width = 304,
			height = 39
		},
		{	-- frame 3 / midFrame (ingredient)
			x = 0,
			y = 74,
			width = 304,
			height = 42
		},
		{	-- frame 4 / bottomFrame (endFrame)
			x = 0,
			y = 254,
			width = 304,
			height = 40
		}		
	},
	sheetContentWidth = 304,
	sheetContentHeight = 278
}

--> Passes what level the player has selected to the composer

local function handleReleaseEvent( event )
    local buttonID = event.target.id
		
	if event.phase == "began" then
	elseif event.phase == "ended" then
		_G.audioButtonSelect()
		if buttonID == "menu" then
			composer.gotoScene( "menu", { effect = "slideRight", time = 300 } )
		elseif buttonID == "play" then
			composer.setVariable( "levelNum", event.target.label)	
			composer.gotoScene( "_game", { effect = "slideLeft", time = 500 } )
		elseif buttonID == "coins" then
			composer.showOverlay( "_shop", { effect = "fade", time = 500, isModal = true } )
		end
	end
end

local function saveCoins()
	coinTxt.text = _G.coinVal
	settings.set('coins', _G.coinVal)
end


-- create()
function scene:create( event )

	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen

    font = "res/fonts/BalooTammudu-Regular.ttf"

    local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
    local mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
	local trashGroup = globals.createDisplayGroup( "trashGroup", displayGroups, sceneGroup )
    
    local bgTop = newImageRectNoDimensions( "res/graphics/menu/menu_top_02.png" )
    globals.setBetterAnchor( bgTop )
    bgTop.x = display.screenOriginX
    bgTop.y = display.screenOriginY - 3
    bgTop.width = display.actualContentWidth
    mainGroup:insert( bgTop )
	
	-- only if iphone X
	if onIphoneX then
		bgTop.height = 80
	end
	
    local bgBottom = newImageRectNoDimensions( "res/graphics/menu/bg_02.png" )
    globals.setBetterAnchor( bgBottom )
    bgBottom.x = display.screenOriginX
    bgBottom.y = bgTop.y + bgTop.contentHeight - 5 --remove noticable padding
	bgBottom.height = display.actualContentHeight -- stretch BG to reach bottom of iphone X screens
	bgBottom.width = display.actualContentWidth -- stretch BG to reach sides of ipad screens
    mainGroup:insert( bgBottom )

    local playBtn = globals.createImageButton( "menu",
                        display.screenOriginX + 10, bgTop.y + bgTop.height - 10,
                        "res/graphics/menu/btnBackUp.png", "res/graphics/menu/btnBackDown.png",
                        mainGroup, handleReleaseEvent, 62, 28 )
    _G.anchor.Center( playBtn )
	playBtn.height = playBtn.height + 5
    playBtn.x = left + ( playBtn.contentWidth * 0.5 ) + 2
    playBtn.y = top + ( playBtn.contentHeight * 0.5 ) + 8
	
	-- only if iphone X
	if onIphoneX then
		playBtn.y = playBtn.y + 20
	end

    local coinsBtn = globals.createImageButton( "coins", 
                        0, bgTop.y + bgTop.height - 10,
                        "res/graphics/game/btnCoinsUp.png", "res/graphics/game/btnCoinsDown.png",
                        mainGroup, handleReleaseEvent, 110, 32)
   	_G.anchor.Center( coinsBtn )
	coinsBtn.x = (display.contentWidth - display.screenOriginX) - ( coinsBtn.contentWidth * 0.5 ) - 2
	coinsBtn.y = playBtn.y

	local noOfLev = levelCount
	for j = 1, noOfLev do
		local rawData = {}
		if j == 1 then
			rawData = {levelNum = j, locked = false, complete = false, productName = menuName, levelName = levelName}
		else
			rawData = {levelNum = j, locked = true, complete = false, productName = menuName, levelName = levelName}
		end
		table.insert(levels, rawData)
	end
	
	local jumpToThisLevel
	
	for i = 1, #_G.dataLevels do
		for j = 1, #levels do 
			if _G.dataLevels[i].soupName == levels[j].productName and 
			_G.dataLevels[i].levelNum == levels[j].levelNum and 
			_G.dataLevels[i].levelName == levels[j].levelName and 
			_G.dataLevels[i].complete == true then
					
				levels[j].complete = _G.dataLevels[i].complete
				if j ~= #levels and _G.dataLevels[i].complete == true then
					levels[j+1].locked = false
					jumpToThisLevel = levels[j+1].levelNum
				end
			end
		end
	end
	
	local function onRowRender( event )
		local menuCardSheet = graphics.newImageSheet("res/graphics/menu/menu_card.png", menuCardOptions  )
		local row = event.row
		local id = event.row.index

		 if (levelTable[id].type == 1 ) then  --> top bar
			local bgrow = display.newImage( menuCardSheet, levelTable[id].type )
			bgrow.x = cX
			bgrow.y = bgrow.contentHeight * 0.5
			row:insert( bgrow )
	    elseif ( levelTable[id].type == 2 ) then  --> soup title
			local bgrow = display.newImage( menuCardSheet, levelTable[id].type )
			bgrow.x = cX
			bgrow.y = bgrow.contentHeight * 0.5
			row:insert( bgrow )
		    local options = {
		        text = treatIngName,
		        font = font,
		        fontSize = 25,
		    }
		    local titleObj = display.newText( options )
		    titleObj.x = cX
		    titleObj.y = bgrow.y + 10
		    if ( _G.mydata[id].locked == 1 ) then
		    	titleObj:setFillColor( GRAY.r, GRAY.g, GRAY.b ) 
		    else
				titleObj:setFillColor(229/255, 0/255, 151/255 )
		    end
		    row:insert( titleObj )
	    elseif ( levelTable[id].type == 3 ) then  --> regular row
			local bgrow = display.newImage( menuCardSheet, levelTable[id].type )
			bgrow.x = cX
			bgrow.y = bgrow.contentHeight * 0.5
			row:insert( bgrow )
			
			local checkboxImg
			if (levels[id-2].complete == true ) then
				checkboxImg = newImageRectNoDimensions( "res/graphics/menu/checkbox_02.png" )
			else
				if (levels[id-2].locked == true ) then
					checkboxImg = newImageRectNoDimensions( "res/graphics/menu/checkbox_00.png" )
				else
					checkboxImg = newImageRectNoDimensions( "res/graphics/menu/checkbox_01.png" )
				end
			end
			
			checkboxImg.x = cX - 110
			checkboxImg.y = bgrow.contentHeight * 0.5
			row:insert(checkboxImg)
			
			local options = {
				text = "Level " .. id-2,
				font = font,
				fontSize = 20
			}
			local name = display.newText( options )
			if ( levels[id-2].locked == true  ) then
		    	name:setFillColor( GRAY.r, GRAY.g, GRAY.b ) 
		    else
		    	name:setFillColor(0, 180/255, 191/255) 
		    end
			name.anchorX = 0 
			name.x = checkboxImg.x + 20
			name.y = checkboxImg.y + 10
			row:insert( name )
			
			local lockImg
			if ( levels[id-2].locked == false ) then
				lockImg = globals.createImageButton( "play", 0, 0,
							"res/graphics/menu/btnplayUp.png", "res/graphics/menu/btnplayDown.png",
							mainGroup, handleReleaseEvent, 62, 37 )
			else
				lockImg = newImageRectNoDimensions( "res/graphics/menu/btnLocked.png" )
			end
			
			lockImg.x = bgrow.x + 100
			lockImg.y = bgrow.y + 3
			lockImg.label = id-2
			row:insert( lockImg )
			
	    elseif ( levelTable[id].type == 4 ) then  --> bottom bar
			local bgrow = display.newImage( menuCardSheet, levelTable[id].type )
			bgrow.x = cX
			bgrow.y = bgrow.contentHeight * 0.5
			row:insert( bgrow )
	    end
	end
	
	local tableView = widget.newTableView({
		left = 0,
		top = bgTop.y + bgTop.height,
		width = display.contentWidth,
		height = display.actualContentHeight - 50,
		hideBackground = true,
		hideScrollBar = true,
		noLines = true,
		onRowRender = onRowRender,
		bottomPadding = 100
	})
	
	mainGroup:insert(tableView)
	
	local isCategory = false
	for i = 1, #levelTable do
		if (levelTable[i].type == 1 ) then
			tableView:insertRow
			{
				rowHeight = 22,
				lineColor = { 0.5, 0.5, 0.5 },
				rowColor = { default={ 1, 1, 1, 0}, over={ 1, 0.5, 0, 0.2 } }
			}
		elseif (levelTable[i].type == 2 ) then
			tableView:insertRow
			{
				rowHeight = 39,
				lineColor = { 0.5, 0.5, 0.5 },
				rowColor = { default={ 1, 1, 1, 0}, over={ 1, 0.5, 0, 0.2 } }
			}
		elseif (levelTable[i].type == 3 ) then
			tableView:insertRow
			{
				rowHeight = 42,
				lineColor = { 0.5, 0.5, 0.5 },
				rowColor = { default={ 1, 1, 1, 0}, over={ 1, 0.5, 0, 0.2 } }
			}
		elseif (levelTable[i].type == 4 ) then
			tableView:insertRow
			{
				rowHeight = 40,
				lineColor = { 0.5, 0.5, 0.5 },
				rowColor = { default={ 0, 0, 0, 0}, over={ 1, 0.5, 0, 0.2 } }
			}
		end
	end
	
	bgTop:toFront() 
	playBtn:toFront()
	coinsBtn:toFront()
	
	if _G.saleShopBool then
		local saleImg = newImageRectNoDimensions("res/graphics/menu/uiSaleGameplay.png")
		saleImg.x = coinsBtn.x  + 48
		saleImg.y = coinsBtn.y - 10
		mainGroup:insert( saleImg )
		
		local goBack = function()
			transition.to(saleImg, {time = 500, xScale = 1, yScale = 1, y =  coinsBtn.y - 10, transition = easing.outElastic})
		end
		
		local doEmphasis = function()
			transition.to(saleImg, {time = 500, xScale = 1.1, yScale = 1.1, y =  coinsBtn.y - 13, transition = easing.inElastic, onComplete = goBack})
		end
		
		doEmphasis()
		timerEmphasis = timer.performWithDelay(3000, doEmphasis, 0)
		timerEmphasis.timerStart = true
	end
	
	
	if jumpToThisLevel ~= nil and levelCount > 10 then
		if jumpToThisLevel <= (levelCount - 7) and jumpToThisLevel >= 6 then
			tableView:scrollToY( { y= -(42*jumpToThisLevel) + 101, time=600} )
		elseif jumpToThisLevel > 13 then
			tableView:scrollToY( { y= -(42*16) + 101, time=600} )
		end
	end
	
	local textSettings =
	{
		text = _G.coinVal,
		x = coinsBtn.x - 10,
		y = coinsBtn.y + 2,
		width = 45,
		height = 30,
		font = font,
		fontSize = 15,
		align = "right"
	}
	
	coinTxt = display.newText (textSettings)
	mainGroup:insert(coinTxt)
end

function scene:updateCoins()
	saveCoins()
end
function scene:showAds()
end
-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
		checkTime()
		if timerEmphasis ~= nil then
			if timerEmphasis.timerStart then
				timer.cancel(timerEmphasis)
				timerEmphasis = nil
			end
		end
        -- Code here runs when the scene is on screen (but is about to go off screen)

    elseif ( phase == "did" ) then
		composer.removeScene( "levelSelect" )
        -- Code here runs immediately after the scene goes entirely off screen
    end
	
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
	print("BABYE")

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene