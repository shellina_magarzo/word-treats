
local M = {}
local widget = require( "widget" )

local function createDisplayGroup( id, displayGroups, sceneGroup )
    local displayGroup = display.newGroup()
    displayGroup.id = id
    sceneGroup:insert( displayGroup )
    table.insert( displayGroups, displayGroup )
    return displayGroup
end

local function getTableMemberWithID( id, t )
    local result = nil
    for i = 1, #t do
        local member = t[ i ]
        if member.id == id then
            result = member
        end
    end
    return result
end

local function togglePanel( id, panels, displayGroups )
    local panel = getTableMemberWithID( id, t )
    local parentGroup = getTableMemberWithID( panel.parentGroupID, displayGroups )

    local visibility = not panel.isVisible
    panel.isVisible = visibility
    for i = 1, parentGroup.numChildren do
        local panelMember = parentGroup[ i ]
        panelMember.isVisible = visibility
    end

    -- TODO: remove listeners outside of panel??
end

local function addListeners( objs )
    for i = 1, #objs do
        local obj = objs[ i ]
        if obj.hasListener then
            obj:addEventListener( obj.listenerOptions.eventName, obj.listenerOptions.listener )
        end
    end
end

local function removeListeners( objs )
    for i = 1, #objs do
        local obj = objs[ i ]
        if obj.hasListener then
            obj:removeEventListener( obj.listenerOptions.eventName, obj.listenerOptions.listener )
        end
    end
end

local function setBetterAnchor( obj )
    obj.anchorX = 0
    obj.anchorY = 0
end

local function createImageButton( id, x, y, defaultFile, overFile, parentGroup, releaseEventListener, width, height)
    local button = widget.newButton
    {
        id = id,
        x = x,
        y = y,
		width = width,
		height = height,
        defaultFile = defaultFile,
        overFile = overFile,
        onRelease = releaseEventListener
    }
    parentGroup:insert( button )
    return button
end

M.getTableMemberWithID = getTableMemberWithID
M.togglePanel = togglePanel
M.createDisplayGroup = createDisplayGroup
M.addListeners = addListeners
M.removeListeners = removeListeners
M.setBetterAnchor = setBetterAnchor
M.createImageButton = createImageButton

return M
