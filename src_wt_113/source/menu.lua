local composer = require( "composer" )
local scene = composer.newScene()
local globals = require( "globals" )
local widget = require( "widget" )

local GRAY = { r = 0.7019607, g = 0.8, b = 0.7019607 }
local BLACK = { r = 0, g = 0.5, b = 0 }

local displayGroups = {}
local panels = {}
local menus = {}


local index = 1

	
local tableView = nil
--> Functions
local drawMenus

_G.lastScene = "menu"

local cX = display.contentCenterX
local top = display.screenOriginY
local left = display.screenOriginX
local bottom = display.contentHeight - display.screenOriginY
local right = display.contentWidth - display.screenOriginX

local coinTxt = nil
local timerEmphasis

local menuCardOptions =
{
	frames = 
	{ 
		{	-- frame 1 / crownFrame
			x = 0,
			y = - 0,
			width = 304,
			height = 22
		},
		{	-- frame 2 / topFrame (title i.e. Prep Cook/Apprentice)
			x = 0,
			y = 22,
			width = 304,
			height = 39
		},
		{	-- frame 3 / midFrame (ingredient)
			x = 0,
			y = 74,
			width = 304,
			height = 42
		},
		{	-- frame 4 / bottomFrame (endFrame)
			x = 0,
			y = 254,
			width = 304,
			height = 40
		}		
	},
	sheetContentWidth = 304,
	sheetContentHeight = 278
}

local function saveCoins()
	coinTxt.text = _G.coinVal
	settings.set('coins', _G.coinVal)
end

--> Passes what levels the player has selected to the composer
local function passLevels(event)
	local target = event.target
	composer.setVariable( "levelCount", _G.mydata[target.index].levelcount )
	composer.setVariable( "letterCount", _G.mydata[target.index].lettercount )
	composer.setVariable( "levelName", _G.mydata[target.index].soupIng )
	composer.setVariable( "treatIngName", _G.mydata[target.index].label ) 
	composer.setVariable( "treatName", _G.mydata[target.index].productName ) 
	composer.setVariable( "menuName", _G.mydata[target.index].soupName )
	composer.setVariable( "dataIndex", target.index)
	composer.setVariable( "wordSet",  _G.mydata[target.index].whichPuzzleSet )
end

local function handleReleaseEvent( event )
    local buttonID = event.target.id
	
	if event.phase == "began" then
	elseif event.phase == "ended" then
		_G.audioButtonSelect()
		if buttonID == "level" then
			passLevels( event )
			composer.gotoScene( "levelSelect", { effect = "slideLeft", time = 300 } )
		elseif buttonID == "play" then
			composer.gotoScene( "_game", { effect = "fade", time = 300 } )
		elseif buttonID == "back" then
			composer.gotoScene( "_mainMenu", { effect = "slideDown", time = 300 } )
		elseif buttonID == "coins" then
			composer.showOverlay( "_shop", { effect = "fade", time = 500, isModal = true } )
		end
	end
end

-- create()
function scene:create( event )
	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen

    font = "res/fonts/BalooTammudu-Regular.ttf"

    local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
    local mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
	local trashGroup = globals.createDisplayGroup( "trashGroup", displayGroups, sceneGroup )
    
    local bgTop = newImageRectNoDimensions( "res/graphics/menu/menu_top_02.png" )
    globals.setBetterAnchor( bgTop )
    bgTop.x = display.screenOriginX    
    bgTop.y = display.screenOriginY - 3
    bgTop.width = display.actualContentWidth

	-- only if iphone X
	if onIphoneX then
		bgTop.height = 80 
	end

    mainGroup:insert( bgTop )	

    local bgBottom = newImageRectNoDimensions( "res/graphics/menu/bg_02.png" )
    globals.setBetterAnchor( bgBottom )
    bgBottom.x = display.screenOriginX
    bgBottom.y = bgTop.y + bgTop.contentHeight - 5 --remove noticable padding
    bgBottom.height = display.actualContentHeight -- stretch BG to reach bottom of iphone X screens
	bgBottom.width = display.actualContentWidth -- stretch BG to reach sides of ipad screens
    mainGroup:insert( bgBottom )

    local backBtn = globals.createImageButton( "back",
                        display.screenOriginX + 10, bgTop.y + bgTop.height - 10,
                        "res/graphics/menu/btnBackUp.png", "res/graphics/menu/btnBackDown.png",
                        mainGroup, handleReleaseEvent, 62, 28)
	_G.anchor.Center( backBtn )
	backBtn.height = backBtn.height + 5
    backBtn.x = left + ( backBtn.contentWidth * 0.5 ) + 2
    backBtn.y = top + ( backBtn.contentHeight * 0.5 ) + 8

	-- only if iphone X
	if onIphoneX then
		backBtn.y = backBtn.y + 20
	end

    local coinsBtn = globals.createImageButton( "coins", 
                        0, bgTop.y + bgTop.height - 10,
                        "res/graphics/game/btnCoinsUp.png", "res/graphics/game/btnCoinsDown.png",
                        mainGroup, handleReleaseEvent, 110, 32)
    _G.anchor.Center( coinsBtn )
	coinsBtn.x = (display.contentWidth - display.screenOriginX) - ( coinsBtn.contentWidth * 0.5 ) - 2
	coinsBtn.y = backBtn.y
	
	if _G.saleShopBool then
		local saleImg = newImageRectNoDimensions("res/graphics/menu/uiSaleGameplay.png")
		saleImg.x = coinsBtn.x  + 48
		saleImg.y = coinsBtn.y - 10
		mainGroup:insert( saleImg )
		
		local goBack = function()
			transition.to(saleImg, {time = 500, xScale = 1, yScale = 1, y = coinsBtn.y - 10, transition = easing.outElastic})
		end
		
		local doEmphasis = function()
			transition.to(saleImg, {time = 500, xScale = 1.1, yScale = 1.1, y = coinsBtn.y - 13, transition = easing.inElastic, onComplete = goBack})
		end
		
		doEmphasis()
		timerEmphasis = timer.performWithDelay(3000, doEmphasis, 0)
		timerEmphasis.timerStart = true
	end
	
	local textSettings =
	{
		text = _G.coinVal,
		x = coinsBtn.x - 10,
		y = coinsBtn.y + 2,
		width = 45,
		height = 30,
		font = font,
		fontSize = 15,
		align = "right"
	}
	
	coinTxt = display.newText (textSettings)
	mainGroup:insert(coinTxt)
	
	local unlocknextStageMenu = false

	local function onRowRender( event )
		local menuCardSheet = graphics.newImageSheet("res/graphics/menu/menu_card.png", menuCardOptions  )
	    local row = event.row
	    local id = row.index

	    if ( _G.mydata[id].type == 1 ) then  --> top bar
			local bgrow = display.newImage( menuCardSheet, _G.mydata[id].type )
			bgrow.x = cX
			bgrow.y = bgrow.contentHeight * 0.5
			row:insert( bgrow )
	    elseif ( _G.mydata[id].type == 2 ) then  --> soup title
			local bgrow = display.newImage( menuCardSheet, _G.mydata[id].type )
			bgrow.x = cX
			bgrow.y = bgrow.contentHeight * 0.5
			row:insert( bgrow )
		    local options = {
		        text = _G.mydata[id].label,
		        font = font,
		        fontSize = 25,
		    }
		    local titleObj = display.newText( options )
		    titleObj.x = cX
		    titleObj.y = bgrow.y + 10
		    if ( _G.mydata[id].locked == 1 ) then
		    	titleObj:setFillColor( GRAY.r, GRAY.g, GRAY.b )
		    else
				titleObj:setFillColor(229/255, 0/255, 151/255 )
		    end
		    row:insert( titleObj )
	    elseif ( _G.mydata[id].type == 3 ) then  --> regular row
			local bgrow = display.newImage( menuCardSheet, _G.mydata[id].type )
			bgrow.x = cX
			bgrow.y = bgrow.contentHeight * 0.5
			row:insert( bgrow )

			local checkboxImg
			if ( _G.mydata[id].complete == 1 ) then
				checkboxImg = newImageRectNoDimensions( "res/graphics/menu/checkbox_02.png" )
			else
				if ( _G.mydata[id].locked == 1 ) then
					checkboxImg = newImageRectNoDimensions( "res/graphics/menu/checkbox_00.png" )
				else
					checkboxImg = newImageRectNoDimensions( "res/graphics/menu/checkbox_01.png" )
				end
			end
			checkboxImg.x = cX - 110
			checkboxImg.y = bgrow.contentHeight * 0.5
			row:insert( checkboxImg )

		    local options = {
		        text = _G.mydata[id].label,
		        font = font,
		        fontSize = 20
		    }
		    local titleObj = display.newText( options )
		    titleObj.x = checkboxImg.x + 15
		    titleObj.y = checkboxImg.y
		    if ( _G.mydata[id].locked == 1 ) then
		    	titleObj:setFillColor( GRAY.r, GRAY.g, GRAY.b ) 
		    else
			    titleObj:setFillColor(229/255, 0/255, 151/255 ) 
			end
		    _G.anchor.CenterLeft( titleObj )
		    row:insert( titleObj )

		    local options = {
		        text = _G.mydata[id].lettercount .. " letters max",
		        font = font,
		        fontSize = 12
		    }
		    local puzzleDetails = display.newText( options )
		    puzzleDetails.x = checkboxImg.x + 15
		    puzzleDetails.y = checkboxImg.y + 15
			if ( _G.mydata[id].locked == 1 ) then
		    	puzzleDetails:setFillColor( GRAY.r, GRAY.g, GRAY.b ) 
		    else
			    puzzleDetails:setFillColor(0, 180/255, 191/255) 
			end
			
		    _G.anchor.CenterLeft( puzzleDetails )
		    row:insert( puzzleDetails )

		    local lockImg
			if ( _G.mydata[id].locked == 0 ) then
				lockImg = globals.createImageButton( "level", 0, 0,
							"res/graphics/menu/btnplayUp.png", "res/graphics/menu/btnplayDown.png",
							mainGroup, handleReleaseEvent, 62, 37 )
			else
				lockImg = newImageRectNoDimensions( "res/graphics/menu/btnLocked.png")
			end
			lockImg.index = id
			lockImg.x = bgrow.x + 100
			lockImg.y = bgrow.y
			row:insert( lockImg )

	    elseif ( _G.mydata[id].type == 4 ) then  --> bottom bar
			local bgrow = display.newImage( menuCardSheet, _G.mydata[id].type )
			bgrow.x = cX
			bgrow.y = bgrow.contentHeight * 0.5
			row:insert( bgrow )
		elseif (_G.mydata[id].type == 5 ) then  --> MORE SOUPS to COME
			local bgrow = newImageRectNoDimensions("res/graphics/menu/speech_bubble.png")
            bgrow.x = cX - (bgrow.contentWidth * 0.5) - 37
			bgrow.y = bgrow.contentHeight * 0.5
			bgrow.width = 210
			bgrow.height = 70
			bgrow.anchorX = 0
			
			local hamsterSheet1Options =
			{
				width = 261,
				height = 269,
				numFrames = 4
			}
			local sheet_hamster1 = graphics.newImageSheet( "res/graphics/game/hamster1SS.png", hamsterSheet1Options )
			local sequences_hamster1 = {
				{
					name = "hamster1",
					start = 1,
					count = 4,
					time = 800,
					loopCount = 0,
					loopDirection = "forward"
				}
			}
			
			local hamster1 = display.newSprite(sheet_hamster1, sequences_hamster1 )
			globals.setBetterAnchor( hamster1 )
			hamster1.x = bgrow.x + bgrow.contentWidth
			hamster1.y = bgrow.y - 58
			hamster1:scale(1/3,1/3)
			hamster1:play()
			
			local msgText = display.newText("More treats coming soon!", bgrow.x + 10, bgrow.y + 5, font, 16)
			msgText:setFillColor(0,0.4,0)
			msgText.anchorX = 0
			
			row:insert (hamster1)
			row:insert( bgrow )
			row:insert (msgText)
	    end
	end

	drawMenus = function()
    	tableView = widget.newTableView({
			left = 0,
			top = bgTop.y + bgTop.height,
			width = display.contentWidth,
			height = display.actualContentHeight - 50,
			bottomPadding = 100,
			hideBackground = true,
			hideScrollBar = true,
			noLines = true,
			onRowRender = onRowRender
		})
		mainGroup:insert( tableView )
		
		local isCategory = false
		for i = 1, #_G.mydata do
			if ( _G.mydata[i].type == 1 ) then
		        tableView:insertRow
		        {
		            rowHeight = 22,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 1, 1, 1, 0}, over={ 1, 0.5, 0, 0.2 } }
		        }
			elseif ( _G.mydata[i].type == 2 ) then
		        tableView:insertRow
		        {
		            rowHeight = 39,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 1, 1, 1, 0}, over={ 1, 0.5, 0, 0.2 } }
		        }
			elseif ( _G.mydata[i].type == 3 ) then
		        tableView:insertRow
		        {
		            rowHeight = 42,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 1, 1, 1, 0}, over={ 1, 0.5, 0, 0.2 } }
		        }
			elseif ( _G.mydata[i].type == 4 ) then
		        tableView:insertRow
		        {
		            rowHeight = 30,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 0, 0, 0, 0}, over={ 1, 0.5, 0, 0.2 } }
		        }
			elseif ( _G.mydata[i].type == 5 ) then
		        tableView:insertRow
		        {
		            rowHeight = 110,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 0, 0, 0, 0}, over={ 1, 0.5, 0, 0.2 } }
		        }
		    end
	    end
	end
		
	drawMenus()
	
	--[[
	if _G.mydata[i].type == 2 and _G.mydata[i].label == "Chocolate Ice Cream" and _G.mydata[i].locked == 0 and _G.doRateUs and _G.rateDone == false then
			timer.performWithDelay(500, function() composer.showOverlay( "popUp_rateUs", { effect = "fade", time = 500,  isModal = true } ) end)
		end --]]

	if _G.doRateUs and _G.rateDone == false then
		timer.performWithDelay(500, function() composer.showOverlay( "popUp_rateUs", { effect = "fade", time = 500,  isModal = true } ) end)
	end
	
	for i = 1, #_G.mydata do 
		if _G.mydata[i].type == 2 and _G.mydata[i].label == "Caramel Sundae" and _G.mydata[i].locked == 0 then
			_G.rewardAdBool = true
			settings.set('rewardAdBool', _G.rewardAdBool)
		end
	end
	
	--> FOR GPGS SAVING ACHIEVEMENT
	-- local function achievementUnlock(id)
		-- if ( _G.whichPlatform == "Android" ) then
			-- Submit a score to Google Play Games Services
			-- _G.gpgs.achievements.unlock(
			-- {
				-- achievementId  = id,
			-- })
			-- print("GPGS UNLOCK ACHIEVEMENT")
		-- end
	-- end
	
	-- local function submitScore(score)
		-- if ( _G.whichPlatform == "Android" ) then
			-- Submit a score to Google Play Games Services
			-- _G.gpgs.leaderboards.submit(
			-- {
				-- score = score,
				-- leaderboardId = "CgkI2JGyxtoCEAIQCg",
			-- })
			-- print("SUBMITTING SCORE TO LEADERBOARD")
		-- end
	-- end
	
	-- for x = 1, #_G.T_achievements do
		-- local treatName = _G.T_achievements[x].treatName
		-- for i = 1, #_G.mydata do 
			-- if _G.mydata[i].type == 3 and _G.mydata[i].productName == treatName and _G.mydata[i].label == _G.T_achievements[x].lastIng and 
				-- _G.mydata[i].locked == 0 and _G.mydata[i].complete == 1 and _G.T_achievements[x].awarded == false then
				-- _G.T_achievements[x].complete = true
				-- if ( _G.loggedIntoGC ) then
					-- achievementUnlock(_G.T_achievements[x].achievementID)
					-- _G.T_achievements[x].awarded = true
					-- settings.set('achievements['..x..']', _G.T_achievements[x])
					
					-- submitScore(x)
				-- end
			-- end
		-- end
	-- end
end

function scene:updateCoins()
	saveCoins()
end
function scene:showAds()
end
-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen

	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		checkTime()
		if timerEmphasis ~= nil then
			if timerEmphasis.timerStart then
				timer.cancel(timerEmphasis)
				timerEmphasis = nil
			end
		end
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		composer.removeScene( "menu" )
		-- Code here runs immediately after the scene goes entirely off screen

	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	print("BABYE")
	-- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene