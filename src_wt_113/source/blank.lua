
-- About this scene: This is a blank lua scene used as transition between two _game.lua scenes as a workaround to prevent bugs
local composer = require( "composer" )

local scene = composer.newScene()

-- -----------------------------------------------------------------------------------
-- Code outside of the scene event functions below will only be executed ONCE unless
-- the scene is removed entirely (not recycled) via "composer.removeScene()"
-- -----------------------------------------------------------------------------------

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
local function loadInterstitialAds()
	print("Ad counter: " .. _G.adCounter)
	if _G.adCounter % 3 == 0 and _G.adCounter ~= 0 then
		if(_G.ads == true ) then
			if ( _G.admob.isLoaded( "interstitial" ) ) then
				if ( _G.admob.isLoaded( "banner" ) ) then
					_G.admob.hide("banner")
				end
				timer.performWithDelay(500, function() _G.admob.show( "interstitial" ) end)
				print ("INTERSTITIAL: Showing Interstitial Ad [Game]")
				_G.adCounter = 0
			else
				print ("INTERSTITIAL: No Interstitial Ad loaded [Game]")	
			end
		end
	end
end

-- create()
function scene:create( event )

	print("create")
	local sceneGroup = self.view
	-- Code here runs when the scene is first created but has not yet appeared on screen
	loadInterstitialAds()
	composer.gotoScene( "_game", { effect = "slideUp", time = 500 } )
end


-- show()
function scene:show( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is still off screen (but is about to come on screen)
		print("show")

	elseif ( phase == "did" ) then
		-- Code here runs when the scene is entirely on screen
		
	end
end


-- hide()
function scene:hide( event )

	local sceneGroup = self.view
	local phase = event.phase

	if ( phase == "will" ) then
		-- Code here runs when the scene is on screen (but is about to go off screen)

	elseif ( phase == "did" ) then
		-- Code here runs immediately after the scene goes entirely off screen
		composer.removeScene( "blank" )
	end
end


-- destroy()
function scene:destroy( event )

	local sceneGroup = self.view
	-- Code here runs prior to the removal of scene's view
	
end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene