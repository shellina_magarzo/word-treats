
-- -----------------------------------------------------------------------------------
-- Shop.lua
-- -----------------------------------------------------------------------------------

local composer = require( "composer" )
local globals = require( "globals" )
local widget = require( "widget" )
local inspect = require( "inspect" )

local displayGroups = {}
local purchaseBtns = {}
local scene = composer.newScene()

local tempCoinVal = _G.coinVal

local parent
_G.shopOpen = true

--> Variables used for in-game store

local defaultProductList = 
{
	"com.popsiclegames.alphabetsoup.iap1", -- 240   coins
	"com.popsiclegames.alphabetsoup.iap2", -- 720   coins
	"com.popsiclegames.alphabetsoup.iap3", -- 1,340 coins
	"com.popsiclegames.alphabetsoup.iap4", -- 2,940 coins
	"com.popsiclegames.alphabetsoup.iap5", -- 6,240 coins
	"com.popsiclegames.alphabetsoup.iap6"  -- Remove Ads
}

local additionalProductList =
{
	"com.popsiclegames.alphabetsoup.iap1b",
	"com.popsiclegames.alphabetsoup.iap2b",
	"com.popsiclegames.alphabetsoup.iap3b",
	"com.popsiclegames.alphabetsoup.iap4b",
	"com.popsiclegames.alphabetsoup.iap5b",
	"com.popsiclegames.alphabetsoup.iap6b",
	"com.popsiclegames.alphabetsoup.iap1", -- 240   coins
	"com.popsiclegames.alphabetsoup.iap2", -- 720   coins
	"com.popsiclegames.alphabetsoup.iap3", -- 1,340 coins
	"com.popsiclegames.alphabetsoup.iap4", -- 2,940 coins
	"com.popsiclegames.alphabetsoup.iap5", -- 6,240 coins
	"com.popsiclegames.alphabetsoup.iap6"  -- Remove Ads
}


local priceList = 
{
	"$0.99",
	"$2.99",
	"$4.99",
	"$9.99",
	"$19.99",
	"$1.99"
}

local discountedPriceList =
{
	"$0.495",
	"$1.495",
	"$2.495",
	"$4.995",
	"$9.995",
	"$0.995"
}

local tempPriceList = {}


if _G.saleShopBool then
	tempPriceList = priceList
	priceList = discountedPriceList
	discountedPriceList = tempPriceList
end

--local store
local isOnline = false

--local targetAppStore = system.getInfo( "targetAppStore" )

--[[ 
if ( "apple" == targetAppStore ) then  -- iOS
    store = require( "store" )
elseif ( "google" == targetAppStore ) then  -- Android
    store = require( "plugin.google.iap.v3" )
elseif ( "amazon" == targetAppStore ) then  -- Amazon
    store = require( "plugin.amazon.iap" )
else
    print( "In-app purchases are not available for this platform." )
end
]]--

local cdTimer
local timerCd
local whichIAP
local showIAPList
_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "viewed_shop", item_id = "1"} )

local top = display.screenOriginY
local left = display.screenOriginX
local bottom = display.contentHeight - display.screenOriginY
local right = display.contentWidth - display.screenOriginX

local function saveCoins()
	coinTxt.text = _G.coinVal
	settings.set('coins', _G.coinVal)
end

--> Give Premium Pack
local function proceedWithPurchase()
	if ( whichIAP == 1 ) then
		_G.coinVal = _G.coinVal + 240
	elseif ( whichIAP == 2 ) then
		_G.coinVal = _G.coinVal + 720
	elseif ( whichIAP == 3 ) then
		_G.coinVal = _G.coinVal + 1340
	elseif ( whichIAP == 4 ) then
		_G.coinVal = _G.coinVal + 2940
	elseif ( whichIAP == 5 ) then
		_G.coinVal = _G.coinVal + 6240
	elseif ( whichIAP == 6 ) then
		_G.ads = false
		settings.set('ads', _G.ads)
	end
	saveCoins()
end

local tempTable = {}
local productIdentifiers = {}


local function loadProductsCallback( event )

    _G.invalidProducts = event.invalidProducts

	-- Used to sort Google Play IAP IN ORDER OF PRICES
	if _G.whichPlatform == "Android" then
		if event.products then
		  table.sort(event.products, function(a,b) 
			if a.priceAmountMicros then
				return ( tonumber(a.priceAmountMicros) < tonumber(b.priceAmountMicros) )
			  end
		  end)
		end
	end
	
	print(#event.products .. " >>> # of products loaded. ") 
	local productList = {}
	_G.validProducts = {}
	
	if _G.saleShopBool then
		productList = additionalProductList
	else
		productList = defaultProductList
	end
	
	local currentProduct
	
	for i = 1, #event.products do 
		currentProduct = event.products[i]
		
		if(currentProduct.productIdentifier == productList[1]) then
			_G.validProducts[1] = {}
			_G.validProducts[1].title = currentProduct.title
			_G.validProducts[1].productIdentifier = currentProduct.productIdentifier
			_G.validProducts[1].localizedPrice = currentProduct.localizedPrice
			_G.validProducts[1].description = currentProduct.description
			print(i)
		elseif(currentProduct.productIdentifier == productList[2]) then
			_G.validProducts[2] = {}
			_G.validProducts[2].title = currentProduct.title
			_G.validProducts[2].productIdentifier = currentProduct.productIdentifier
			_G.validProducts[2].localizedPrice = currentProduct.localizedPrice
			_G.validProducts[2].description = currentProduct.description
			print(i)
		elseif(currentProduct.productIdentifier == productList[3]) then
			_G.validProducts[3] = {}
			_G.validProducts[3].title = currentProduct.title
			_G.validProducts[3].productIdentifier = currentProduct.productIdentifier
			_G.validProducts[3].localizedPrice = currentProduct.localizedPrice
			_G.validProducts[3].description = currentProduct.description
			print(i)
		elseif(currentProduct.productIdentifier == productList[4]) then
			_G.validProducts[4] = {}
			_G.validProducts[4].title = currentProduct.title
			_G.validProducts[4].productIdentifier = currentProduct.productIdentifier
			_G.validProducts[4].localizedPrice = currentProduct.localizedPrice
			_G.validProducts[4].description = currentProduct.description
			print(i)
		elseif(currentProduct.productIdentifier == productList[5]) then
			_G.validProducts[5] = {}
			_G.validProducts[5].title = currentProduct.title
			_G.validProducts[5].productIdentifier = currentProduct.productIdentifier
			_G.validProducts[5].localizedPrice = currentProduct.localizedPrice
			_G.validProducts[5].description = currentProduct.description
			print(i)
		elseif(currentProduct.productIdentifier == productList[6]) then
			_G.validProducts[6] = {}
			_G.validProducts[6].title = currentProduct.title
			_G.validProducts[6].productIdentifier = currentProduct.productIdentifier
			_G.validProducts[6].localizedPrice = currentProduct.localizedPrice
			_G.validProducts[6].description = currentProduct.description
			print(i)
		elseif(currentProduct.productIdentifier == productList[7]) then
			_G.validProducts[7] = {}
			_G.validProducts[7].title = currentProduct.title
			_G.validProducts[7].productIdentifier = currentProduct.productIdentifier
			_G.validProducts[7].localizedPrice = currentProduct.localizedPrice
			_G.validProducts[7].description = currentProduct.description
			print(i)
		elseif(currentProduct.productIdentifier == productList[8]) then
			_G.validProducts[8] = {}
			_G.validProducts[8].title = currentProduct.title
			_G.validProducts[8].productIdentifier = currentProduct.productIdentifier
			_G.validProducts[8].localizedPrice = currentProduct.localizedPrice
			_G.validProducts[8].description = currentProduct.description
			print(i)
		elseif(currentProduct.productIdentifier == productList[9]) then
			_G.validProducts[9] = {}
			_G.validProducts[9].title = currentProduct.title
			_G.validProducts[9].productIdentifier = currentProduct.productIdentifier
			_G.validProducts[9].localizedPrice = currentProduct.localizedPrice
			_G.validProducts[9].description = currentProduct.description
			print(i)
		elseif(currentProduct.productIdentifier == productList[10]) then
			_G.validProducts[10] = {}
			_G.validProducts[10].title = currentProduct.title
			_G.validProducts[10].productIdentifier = currentProduct.productIdentifier
			_G.validProducts[10].localizedPrice = currentProduct.localizedPrice
			_G.validProducts[10].description = currentProduct.description
			print(i)
		elseif(currentProduct.productIdentifier == productList[11]) then
			_G.validProducts[11] = {}
			_G.validProducts[11].title = currentProduct.title
			_G.validProducts[11].productIdentifier = currentProduct.productIdentifier
			_G.validProducts[11].localizedPrice = currentProduct.localizedPrice
			_G.validProducts[11].description = currentProduct.description
			print(i)
		elseif(currentProduct.productIdentifier == productList[12]) then
			_G.validProducts[12] = {}
			_G.validProducts[12].title = currentProduct.title
			_G.validProducts[12].productIdentifier = currentProduct.productIdentifier
			_G.validProducts[12].localizedPrice = currentProduct.localizedPrice
			_G.validProducts[12].description = currentProduct.description
			print(i)
		end
		if i == #event.products then
			timer.performWithDelay(500, showIAPList)
		end
	end
	
	if _G.validProducts ~= nil then
		print("LOADING PRODUCTS")
        for i = 1, #_G.validProducts do
			print(_G.validProducts[i].title )
			print("DESCRIPTION: " .. _G.validProducts[i].description)
			print("PRICE: " .. _G.validProducts[i].localizedPrice )
			print("PRODUCT ID: " .. _G.validProducts[i].productIdentifier )
			print("======================================================" )
        end
		_G.productsLoaded = true
		settings.set('validProducts', _G.validProducts)
	else
		_G.validProducts = {}
		print("THERE ARE NO VALID PRODUCTS")
    end
	
	
    for j = 1,#_G.invalidProducts do 
		print("INVALID PROD: " .. _G.invalidProducts[j] )
    end
end
-------------------------------------------------------------------------------
-- Handler for all store transactions
-- This callback is set up by store.init()
-------------------------------------------------------------------------------

local function mystoreCallbackGoogle( event )
    -- Google IAP initialization event
    if ( event.name == "init" ) then
        if not ( event.transaction.isError ) then
          -- Perform steps to enable IAP, load products, etc.
          --> NOTE: "canLoadProducts" is not supported in Google Play Store
			if ( _G.store.canLoadProducts ) then
				if _G.saleShopBool then
					_G.store.loadProducts ( additionalProductList, loadProductsCallback )
				else
					_G.store.loadProducts ( defaultProductList, loadProductsCallback )
				end
			end
        else  -- Unsuccessful initialization; output error details
            print( event.transaction.errorType )
            print( event.transaction.errorString )
        end
    -- Store transaction event
    elseif ( event.name == "storeTransaction" ) then
		if event.transaction.state == "purchased" then
			proceedWithPurchase()
			_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "bought_IAP" .. whichIAP, item_id = whichIAP } )
			
			--[[
			if tempCoinVal == _G.coinVal then
				proceedWithPurchase()
				tempCoinVal = _G.coinVal
			end	
			]]--
			
			native.showAlert( _G.gametitle, "Thank you for your support!", { "OK" } )
		elseif  event.transaction.state == "restored" then
			-- Reminder: your app must store this information somewhere
			-- Here we just display some of it
			_G.ads = false
			settings.set('ads', _G.ads)
			print("receipt", event.transaction.receipt)
			print("transactionIdentifier", event.transaction.transactionIdentifier)
			print("date", event.transaction.date)
			print("originalReceipt", event.transaction.originalReceipt)
			native.showAlert( _G.gametitle, "Your previous purchase was successfully restored. Happy playing!", { "OK" } )
			--> MISSING: Restoring an in-app item should be handled differently, this is only applicable for the REMOVE ADS IAP
		elseif event.transaction.state == "consumed" then
			print( "Transaction consumed!" )
			print( "product identifier", event.transaction.productIdentifier )
			print( "receipt", event.transaction.receipt )
			print( "transaction identifier", event.transaction.identifier )
			print( "date", event.transaction.date )
			print( "original receipt", event.transaction.originalReceipt )
			print( "original transaction identifier", event.transaction.originalIdentifier )
			print( "original date", event.transaction.originalDate )
		elseif event.transaction.state == "cancelled" then
		elseif event.transaction.state == "failed" then
			native.showAlert( _G.gametitle, "Transaction cancelled.", { "OK" } )
			native.showAlert( _G.gametitle, "Failed! " .. event.transaction.errorType .. " " .. event.transaction.errorString .. ".", { "OK" } )
		else
			native.showAlert( _G.gametitle, "Unknown Error!", { "OK" } )
		end
		-- Tell the store we are done with the transaction.
		-- If you are providing downloadable content, do not call this until
		-- the download has completed.
		_G.store.finishTransaction( event.transaction )
		native.setActivityIndicator( false )

		--> Make item available for purchase again
		if ( _G.whichPlatform == "Android" ) and whichIAP ~= 6 then
			--> Why timer? FIX
			timer.performWithDelay(2000, _G.store.consumePurchase(_G.validProducts[whichIAP].productIdentifier))
		end
	end
end

-------------------------------------------------------------------------------
-- Handler for all store transactions
-- This callback is set up by store.init()
-------------------------------------------------------------------------------
local function mystoreCallbackAmazon(event)
	local transaction = event.transaction
 
    if ( transaction.isError ) then
        print( transaction.errorType )
        print( transaction.errorString )
    else
        -- No errors; proceed
        if ( transaction.state == "purchased" or transaction.state == "restored" ) then
            -- Handle a normal purchase or restored purchase here
			proceedWithPurchase()
			_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "bought_IAP" .. whichIAP, item_id = whichIAP } )
			
			--[[
			if tempCoinVal == _G.coinVal then
				proceedWithPurchase()
				tempCoinVal = _G.coinVal
			end	
			]]--

			native.showAlert( _G.gametitle, "Thank you for your support!", { "OK" } )
			
            print( transaction.state )
            print( transaction.productIdentifier )
            print( transaction.date )
 
        elseif ( transaction.state == "cancelled" ) then
            -- Handle a cancelled transaction here
			native.showAlert( _G.gametitle, "Transaction cancelled.", { "OK" } )
 
        elseif ( transaction.state == "failed" ) then
            -- Handle a failed transaction here
			native.showAlert( _G.gametitle, "Failed! " .. event.transaction.errorType .. " " .. event.transaction.errorString .. ".", { "OK" } )
        end
 
        -- Tell the store that the transaction is complete
        -- If you're providing downloadable content, do not call this until the download has completed
        _G.store.finishTransaction( transaction )
    end
end


local function mystoreCallback( event )
	if event.transaction.state == "purchased" then
		proceedWithPurchase()
		_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "bought_IAP" .. whichIAP, item_id = whichIAP } )
		--[[
		if tempCoinVal == _G.coinVal then
			proceedWithPurchase()
			tempCoinVal = _G.coinVal
		end	
		]]--
		native.showAlert( _G.gametitle, "Thank you for your support!", { "OK" } )
	elseif  event.transaction.state == "restored" then
		-- Reminder: your app must store this information somewhere
		-- Here we just display some of it
		_G.ads = false
		settings.set('ads', _G.ads)
		print("receipt", event.transaction.receipt)
		print("transactionIdentifier", event.transaction.transactionIdentifier)
		print("date", event.transaction.date)
		print("originalReceipt", event.transaction.originalReceipt)
		native.showAlert( _G.gametitle, "Your previous purchase was successfully restored. Happy playing!", { "OK" } )
		--> MISSING: Restoring an in-app item should be handled differently, this is only applicable for the REMOVE ADS IAP
	elseif event.transaction.state == "consumed" then
		print( "Transaction consumed!" )
		print( "product identifier", event.transaction.productIdentifier )
		print( "receipt", event.transaction.receipt )
		print( "transaction identifier", event.transaction.identifier )
		print( "date", event.transaction.date )
		print( "original receipt", event.transaction.originalReceipt )
		print( "original transaction identifier", event.transaction.originalIdentifier )
		print( "original date", event.transaction.originalDate )
	elseif event.transaction.state == "cancelled" then
		native.showAlert( _G.gametitle, "Transaction cancelled.", { "OK" } )
	elseif event.transaction.state == "failed" then
		native.showAlert( _G.gametitle, "Failed! " .. event.transaction.errorType .. " " .. event.transaction.errorString .. ".", { "OK" } )
	else
		native.showAlert( _G.gametitle, "Unknown Error!", { "OK" } )
	end
	-- Tell the store we are done with the transaction.
	-- If you are providing downloadable content, do not call this until
	-- the download has completed.
	_G.store.finishTransaction( event.transaction )
	native.setActivityIndicator( false )
end

--> Init store once only!
local function setupStore()
	-- Connect to store at startup
	if "apple" == _G.targetAppStore then
		_G.store.init ( "apple", mystoreCallback )
		if ( _G.store.canLoadProducts ) then
			if _G.saleShopBool then
				_G.store.loadProducts ( additionalProductList, loadProductsCallback )
			else
				_G.store.loadProducts ( defaultProductList, loadProductsCallback )
			end
		end
	elseif "google" == _G.targetAppStore then
		_G.store.init ( mystoreCallbackGoogle )
		print("SHOP: GOOGLE PRODUCTS HAS BEEN LOADED.")
	elseif "amazon" == _G.targetAppStore then
		_G.store.init( mystoreCallbackAmazon )
		if ( _G.store.canLoadProducts ) then
			if _G.saleShopBool then
				_G.store.loadProducts ( additionalProductList, loadProductsCallback )
			else
				_G.store.loadProducts ( defaultProductList, loadProductsCallback )
			end
		end
	else
		print( "In-app purchases are not supported on this system/device." )
	end
end

local function onCompleteRestore( event )
	_G.audioButtonSelect()
	_G.store.restore()
	_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "restored_purchase", item_id = "1"} )

end

local function onCompletePurchase( event )
	_G.audioButtonSelect()
	whichIAP = event.target.id
	if event.phase == "began" then
		
	elseif event.phase == "ended" or event.phase == "press" then
		print("A button has been pressed. Beep boop boop beep.") 
		
		if system.getInfo( "environment" ) == "simulator" then
			proceedWithPurchase()
			_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "bought_IAP" .. whichIAP, item_id = whichIAP } )
			
			--[[
			if tempCoinVal == _G.coinVal then
				proceedWithPurchase()
				tempCoinVal = _G.coinVal
			end	
			]]--
		else
			if ( _G.validProducts ~= nil ) then
				--> Purchase IAP item here!
				if _G.store.canMakePurchases then
					native.setActivityIndicator( true )
					if ( _G.whichPlatform == "Android" ) then
						_G.store.purchase(_G.validProducts[whichIAP].productIdentifier  )
						print("IM BUYING THIS PACKAGE IN ANDROID")
					elseif ( _G.whichPlatform == "iPhone OS" ) then
	--					_G.store.purchase(_G.validProducts[whichIAP].productIdentifier  )
						_G.store.purchase( { _G.validProducts[whichIAP].productIdentifier } )
						print("IM BUYING THIS PACKAGE IN IOS")
					end
				end
			end
		end		
	end
end

local function handleReleaseEvent( event )
	_G.audioButtonSelect()
    local buttonID = event.target.id
	if event.phase == "began" then
	elseif event.phase == "ended" then
		if buttonID == "back" then
			composer.hideOverlay("fade", 500 )
		end
	end
end

local coinFilePath = "res/graphics/menu/coin.png"

local menuCardOptions =
{
	frames = 
	{ 
		{	-- frame 1 / crownFrame
			x = 0,
			y = - 0,
			width = 304,
			height = 22
		},
		{	-- frame 2 / topFrame (title i.e. Prep Cook/Apprentice)
			x = 0,
			y = 22,
			width = 304,
			height = 93
		},
		{	-- frame 3 / midFrame (ingredient)
			x = 0,
			y = 160,
			width = 304,
			height = 60
		},
		{	-- frame 4 / bottomFrame (endFrame)
			x = 0,
			y = 254,
			width = 304,
			height = 40
		}		
	},
	sheetContentWidth = 304,
	sheetContentHeight = 278
}

local shopData = {}
for i = 1, 10 do
 shopData[i] = {}
end

shopData[1] = { type = 1 }
shopData[2] = { type = 2 }
shopData[3] = { type = 3, boolCoin = true, imgPath="res/graphics/menu/coins240.png",    value="240", desc1="A handful of", desc2="coins" }
shopData[4] = { type = 3, boolCoin = true, imgPath="res/graphics/menu/coins720.png",  value="720",   desc1="A hanky of",   desc2="coins" }
shopData[5] = { type = 3, boolCoin = true, imgPath="res/graphics/menu/coins1340.png",   value="1340",desc1="A medium",     desc2="bowl of coins" }
shopData[6] = { type = 3, boolCoin = true, imgPath="res/graphics/menu/coins2940.png",   value="2940",desc1="A large bowl", desc2="of coins" }
shopData[7] = { type = 3, boolCoin = true, imgPath="res/graphics/menu/coins6240.png", value="6240",  desc1="A sack of",    desc2="coins" }
shopData[8] = { type = 3, boolCoin = false, imgPath="res/graphics/menu/noAds.png", desc1="Remove Ads"}
shopData[9] = { type = 3, boolCoin = false}
shopData[10] = { type = 4 }

local function createIAPbuttons(i)
    local mainGroup = globals.getTableMemberWithID( "mainGroup", displayGroups )
	
	local defFile 
	if isOnline then
		defFile = "res/graphics/menu/btnUp.png"
	else
		defFile = "res/graphics/menu/btnUpGray.png"
	end
	
	local offsetY = 0
	if _G.saleShopBool then
		offsetY = 8
	end
	
	local purchaseBtn = widget.newButton(
    {
        id = i,
        label = "BUY",
		font = font,
		fontSize = 13,
		labelColor = { default={ 1,1,1 }, over={ 1, 1, 0, 0.8 } },
		labelYOffset = offsetY,
        onRelease = onCompletePurchase,
		parentGroup = mainGroup,
		defaultFile = defFile,
		overFile =  "res/graphics/menu/btnDown.png"
    })

	if not isOnline then
		purchaseBtn:setEnabled(false)
	end
	
	purchaseBtn.anchorY = 1
	table.insert( purchaseBtns, purchaseBtn )
end

local function onRowTouch ( event )
	local index = event.target.index
	if event.phase == "press" and (index >= 3 and index <= 8) then
		if index == 3 then
			event.target.id = 1
		elseif index == 4 then
			event.target.id = 2
		elseif index == 5 then
			event.target.id = 3
		elseif index == 6 then
			event.target.id = 4
		elseif index == 7 then
			event.target.id = 5
		elseif index == 8 then
			event.target.id = 6
		end
		onCompletePurchase(event)
		print("Buy Product #" .. event.target.id)
	end
	
end


local function onRowRender( event )
	local mainGroup = globals.getTableMemberWithID( "mainGroup", displayGroups )
	
	local imgName = "menu_card.png"
	if _G.saleShopBool then
		imgName = "menu_cardSale.png"
	end
	
	local menuCardSheet = graphics.newImageSheet("res/graphics/menu/"..imgName, menuCardOptions  )
	local row = event.row
	local id = event.row.index
	
	if (shopData[id].type == 1 ) then  --> top bar
		local bgrow = display.newImage( menuCardSheet, shopData[id].type )
		bgrow.x = display.contentCenterX
		bgrow.y = bgrow.contentHeight * 0.5
		row:insert( bgrow )
	elseif ( shopData[id].type == 2 ) then  --> title
		local bgrow = display.newImage( menuCardSheet, shopData[id].type )
		bgrow.x = display.contentCenterX
		bgrow.y = bgrow.contentHeight * 0.5
		row:insert( bgrow )
		local options = {
			text = "Coin Shop",
			font = font,
			fontSize = 25,
		}
		local titleObj = display.newText( options )
		titleObj.x = display.contentCenterX
		titleObj.y = bgrow.y - 15
		titleObj:setFillColor(0,0.2,0)
		row:insert( titleObj )
		
		if _G.saleShopBool then
			local options2 = {
				text = "Check out our limited time deals!",
				font = font,
				fontSize = 15
			}
			local msg = display.newText(options2)
			msg.x = display.contentCenterX
			msg.y = bgrow.y + 5
			msg:setFillColor(0,0.5,0)
			row:insert( msg )
		
			checkTime()
			
			local cdHour, cdMin, cdSec = 0,10,0
			cdHour = 22 - _G.chour
			cdMin = 59 - _G.cmin
			cdSec = 59 - _G.csec
			
			local timeStr = "TIME LEFT: " .. tostring(string.format("%02d", cdHour).. ":" .. string.format("%02d", cdMin) .. ":" .. string.format("%02d", cdSec))

			local textSettings =
			{
				text = timeStr,
				x = display.contentCenterX,
				y = msg.y + 13,
				font = font,
				fontSize = 12,
				align = "center",
			}
			cdTimer = display.newText(textSettings)
			cdTimer:setFillColor(1,0,0)
			row:insert( cdTimer )
			
			local function updateTime ()
				t = os.date('*t')
				
				if cdSec > 0 then
					cdSec = cdSec - 1
				else
					if cdMin > 0 then
						cdSec = 59
						cdMin = cdMin - 1
					else
						if cdHour > 0 then
							cdMin = 59
							cdHour = cdHour - 1
						end
					end
				end
			
				timeStr = "TIME LEFT: " .. tostring(string.format("%02d", cdHour).. ":" .. string.format("%02d", cdMin) .. ":" .. string.format("%02d", cdSec))
				cdTimer.text = timeStr
				
				if cdSec == 0 and cdMin == 0 and cdHour == 0 then
					timer.cancel(timerCd)
					_G.saleShopBool = false
					settings.set('saleShopBool', _G.saleShopBool)
					native.setActivityIndicator(true)
					timer.performWithDelay(1000, function() composer.showOverlay( "_shop", {time = 500,  isModal = true } ) end )
					composer.hideOverlay("fade", 500 )
				end
			end	
			
			timerCd = timer.performWithDelay(1000, updateTime, 0)
			
		end
		
	elseif ( shopData[id].type == 3 ) then  --> regular row
		local bgrow = display.newImage( menuCardSheet, shopData[id].type )
		bgrow.x = display.contentCenterX
		bgrow.y = bgrow.contentHeight * 0.5
		row:insert( bgrow )
		
		local imgPack
		if shopData[id].imgPath ~= nil then
			imgPack = newImageRectNoDimensions(shopData[id].imgPath)
			imgPack.x = display.contentCenterX - 90
			imgPack.y = bgrow.contentHeight * 0.5
			row:insert( imgPack )
		end
		
		if shopData[id].boolCoin then
			local coinImg = newImageRectNoDimensions(coinFilePath)
			coinImg.x = imgPack.x + 50
			coinImg.y = imgPack.y - 13
			row:insert( coinImg )
			
			local options = {
				text = shopData[id].value,
				font = font,
				fontSize = 18,
			}
			
			local titleObj = display.newText( options )  	
			titleObj.x = coinImg.x  + 15
			titleObj.y = coinImg.y + 4
			titleObj:setFillColor(0,0.4,0)
			titleObj.anchorX = 0
			row:insert( titleObj )
			
			local desc1Options = {
				text = shopData[id].desc1,
				font = font,
				fontSize = 14,
			}
			
			local msgDesc1 = display.newText( desc1Options )  
			msgDesc1.x = titleObj.x - 20
			msgDesc1.y = titleObj.y + 15
			msgDesc1.anchorX = 0
			msgDesc1:setFillColor(0.2,0.5,0.2)
			row:insert( msgDesc1 )
			
			local desc2Options = {
				text = shopData[id].desc2,
				font = font,
				fontSize = 14,
			}
			
			local msgDesc2 = display.newText( desc2Options )  
			msgDesc2.x = msgDesc1.x
			msgDesc2.y = msgDesc1.y + 15
			msgDesc2.anchorX = 0
			msgDesc2:setFillColor(0.2,0.5,0.2)
			row:insert( msgDesc2 )
			
			local button = purchaseBtns[id-2]
			button.y = imgPack.y + 20
			button.x = titleObj.x  + 110
			row:insert( button )
			
			local prevPriceOptions
			local prevPrice
			
			if _G.saleShopBool then
				prevPriceOptions = {
					text = discountedPriceList[id-2],
					font = font,
					fontSize = 11
				}
				
				prevPrice = display.newText(prevPriceOptions)
				prevPrice.x = button.x
				prevPrice.y = button.y - button.height + 13
				prevPrice.anchorX = 0.5
				prevPrice:setFillColor(1,1,1,0.8)
				row:insert( prevPrice )
				
				local line = display.newLine(220, button.y-30, 275, button.y-30)
				line:setStrokeColor( 0.9,0.9,0.9,1)
				line.strokeWidth = 1

				row:insert( line )	
			end
			
			if ( system.getInfo( "environment" ) == "simulator" ) then
				if ( id-2 == 1 ) then
					button:setLabel(priceList[id-2])
				elseif ( id-2 == 2 ) then
					button:setLabel(priceList[id-2])
				elseif ( id-2 == 3 ) then
					button:setLabel(priceList[id-2])
				elseif ( id-2 == 4 ) then
					button:setLabel(priceList[id-2])
				elseif ( id-2 == 5 ) then
					button:setLabel(priceList[id-2])
				end
			else
				if isOnline then
					button:setLabel( _G.validProducts[id-2].localizedPrice )
					if _G.saleShopBool then
						prevPrice.text = _G.validProducts[id+4].localizedPrice
					end
				else
					if _G.validProducts == nil then
						button:setLabel(priceList[id-2])
					else
						button:setLabel( _G.validProducts[id-2].localizedPrice )
					end
				end
			end
		end
		
		if id == 8 then
			local desc1Options = {
				text = shopData[id].desc1,
				font = font,
				fontSize = 16,
			}
			
			local msgDesc1 = display.newText( desc1Options )  
			msgDesc1.x = imgPack.x + 30
			msgDesc1.y = imgPack.y  + 5
			msgDesc1.anchorX = 0
			msgDesc1:setFillColor(0.5,0.2,0.2)
			row:insert( msgDesc1 )
			
			local prevPriceOptions
			local prevPrice
			
			if _G.ads == true then
				local button = purchaseBtns[id-2]
				button.y = imgPack.y + 20
				button.x = imgPack.x + 175
				row:insert( button )
				
				if _G.saleShopBool then
					prevPriceOptions = {
					text = discountedPriceList[id-2],
					font = font,
					fontSize = 11
					}
					
					prevPrice = display.newText(prevPriceOptions)
					prevPrice.x = button.x
					prevPrice.y = button.y - button.height + 13
					prevPrice.anchorX = 0.5
					prevPrice:setFillColor(1,1,1,0.8)
					row:insert( prevPrice )
					
					local line = display.newLine(220, button.y-30, 275, button.y-30)
					line:setStrokeColor( 0.9,0.9,0.9,1)
					line.strokeWidth = 1
					row:insert( line )	
				end
			
				--> Show something when in simulator mode
				if ( system.getInfo( "environment" ) == "simulator" ) then
					if ( id-2 == 6 ) then
						purchaseBtns[id-2]:setLabel(priceList[id-2])
					end
				else --> Show localized prices on device mode
					if isOnline then
						purchaseBtns[id-2]:setLabel( _G.validProducts[id-2].localizedPrice )
						if _G.saleShopBool then
							prevPrice.text = _G.validProducts[id+4].localizedPrice
						end
					else
						if _G.validProducts == nil then
							purchaseBtns[id-2]:setLabel(priceList[id-2])
						else
							purchaseBtns[id-2]:setLabel( _G.validProducts[id-2].localizedPrice )
						end
					end
				end
				
			else
				local msgPurchased = display.newEmbossedText("Purchased!", msgDesc1.x + 100, 30, font, 16)
				msgPurchased:setFillColor(0.9,0.4,0.4,1)
				msgPurchased.rotation = -20
				globals.setBetterAnchor(msgPurchased)
				row:insert(msgPurchased)
			end
		elseif id == 9 then
			local defFile 
			
			if isOnline then
				defFile = "res/graphics/menu/btnRestorePurchasesUp.png"
			else
				defFile = "res/graphics/menu/btnRestorePurchasesUpGray.png"
			end
			
			local restoreBtn = globals.createImageButton( "restorePurchase",
						display.contentCenterX,  30,
						defFile, "res/graphics/menu/btnRestorePurchasesDown.png",
						mainGroup, onCompleteRestore, 173, 36 )
			if not isOnline then
				restoreBtn:setEnabled(false)
			end		
			if ( system.getInfo( "platform" ) == "android" ) then
				restoreBtn.alpha = 0
				restoreBtn:setEnabled( false )
			end	
			row:insert(restoreBtn)
		end
		
	elseif ( shopData[id].type == 4 ) then  --> bottom bar
		local bgrow = display.newImage( menuCardSheet, shopData[id].type )
		bgrow.x = display.contentCenterX
		bgrow.y = bgrow.contentHeight * 0.5
		row:insert( bgrow )
	end
end

local tableView



-- create()
function scene:create( event )

	local sceneGroup = self.view
	
    local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
    local mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
	local popUpGroup = globals.createDisplayGroup( "popUpGroup", displayGroups, sceneGroup )
   
	font = "res/fonts/BalooTammudu-Regular.ttf"

	isOnline = testNetworkConnection()
	print (isOnline)
	_G.admob.hide()
	print ("BANNER: Hiding Banner Ad [Shop]")
	_G.bannerVisible = false
	
    local bgTop = newImageRectNoDimensions( "res/graphics/menu/menu_top_02.png" )
    globals.setBetterAnchor( bgTop )
    bgTop.x = display.screenOriginX
    bgTop.y = display.screenOriginY - 3
	bgTop.width = display.actualContentWidth

	-- only if iphone X
	if onIphoneX then
		bgTop.height = 80 
	end

    mainGroup:insert( bgTop )

	local bgPaper = newImageRectNoDimensions( "res/graphics/menu/bgPaper.png" )
    globals.setBetterAnchor( bgPaper )
    bgPaper.x = display.contentCenterX - bgPaper.width/2
    bgPaper.y = bgTop.y + bgTop.contentHeight + 10 --remove noticable padding
	
	bgPaper.isVisible = false
	
	local bgBottom = newImageRectNoDimensions( "res/graphics/menu/bg_02.png" )
	bgBottom.width = display.actualContentWidth
	bgBottom.height = display.actualContentHeight
    globals.setBetterAnchor( bgBottom )
    bgBottom.x = display.screenOriginX
    bgBottom.y = bgTop.y + bgTop.contentHeight - 5 --remove noticable padding
    mainGroup:insert( bgBottom )
	
	local coinsBtn = globals.createImageButton( "coins", 
                        0, bgTop.y + bgTop.height - 10,
                        "res/graphics/game/btn_CoinsUpOff.png", "res/graphics/game/btn_CoinsUpOff.png",
                        mainGroup, handleReleaseEvent, 110, 32 )
	coinsBtn:setEnabled(false)
	
	local backBtn = globals.createImageButton( "back",
                        display.screenOriginX + 10, bgTop.y + bgTop.height - 10,
                        "res/graphics/menu/btnBackUp.png", "res/graphics/menu/btnBackDown.png",
                        mainGroup, handleReleaseEvent, 62, 28)
    _G.anchor.Center( backBtn )
    backBtn.height = backBtn.height + 5
    backBtn.x = left + ( backBtn.contentWidth * 0.5 ) + 2
    backBtn.y = top + ( backBtn.contentHeight * 0.5 ) + 8
		
	-- only if iphone X
	if onIphoneX then
		backBtn.y = backBtn.y + 20
	end
	
	_G.anchor.Center( coinsBtn )
	coinsBtn.x = (display.contentWidth - display.screenOriginX) - ( coinsBtn.contentWidth * 0.5 ) - 2
	coinsBtn.y = backBtn.y
	
	local textSettings =
	{
		text = _G.coinVal,
		x = coinsBtn.x - 7,
		y = coinsBtn.y + 2,
		width = 45,
		height = 30,
		font = font,
		fontSize = 15,
		align = "right"
	}
	
	coinTxt = display.newText (textSettings)
	mainGroup:insert(coinTxt)
	
	showIAPList = function()
		local buttonNum = 6
		if _G.ads == false then
			buttonNum = 5
		end
	
		for i = 1, buttonNum do
			createIAPbuttons(i)
		end
		
		tableView = widget.newTableView({
			left = 0,
			top = bgTop.y + bgTop.height,
			width = display.contentWidth,
			height = display.actualContentHeight - 50,
			hideBackground = true,
			hideScrollBar = true,
			noLines = true,
			isLocked = true,
			onRowRender = onRowRender,
			onRowTouch = onRowTouch
		})
		
		mainGroup:insert(tableView)
		
		local defHeightRow2 = 50
		if _G.saleShopBool then
			defHeightRow2 = 69
		end
		
		local isCategory = false
		for i = 1, #shopData do
			if (shopData[i].type == 1 ) then
				tableView:insertRow
				{
					rowHeight = 22,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 1, 1, 1, 0}, over={ 1, 0.5, 0, 0.2 } }
				}
			elseif (shopData[i].type == 2 ) then
				tableView:insertRow
				{
					rowHeight = defHeightRow2,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 1, 1, 1, 0}, over={ 1, 0.5, 0, 0.2 } }
				}
			elseif (shopData[i].type == 3 ) then
				tableView:insertRow
				{
					rowHeight = 60,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 1, 1, 1, 0}, over={ 1, 0.5, 0, 0.2 } }
				}
			elseif (shopData[i].type == 4 ) then
				tableView:insertRow
				{
					rowHeight = 40,
					lineColor = { 0.5, 0.5, 0.5 },
					rowColor = { default={ 0, 0, 0, 0}, over={ 1, 0.5, 0, 0.2 } }
				}
			end
		end
		
		if ( system.getInfo( "environment" ) == "simulator" ) then
			native.setActivityIndicator(false)
		end
		native.setActivityIndicator(false)
	end
	
	local myRoundedRect, text
	
	local function showConnection() 
		myRoundedRect = display.newRoundedRect( display.contentCenterX, display.contentCenterY, display.actualContentWidth - 80, 100, 15 )
		myRoundedRect.strokeWidth = 3
		myRoundedRect:setStrokeColor( 0.1, 0.4, 0.4 )
		myRoundedRect:scale (0.001,0.001)
		
		popUpGroup:insert(myRoundedRect)
		
		text = display.newText ("Please connect to the Internet to access the shop.", myRoundedRect.x + 5, myRoundedRect.y + 20, myRoundedRect.width - 50, myRoundedRect.height, font, 15)
		text:setFillColor(0,0,0)
		popUpGroup:insert(text)
		text.alpha = 0
		
		transition.to(myRoundedRect, {time = 300, xScale = 1, yScale = 1, transition = easing.inOutBack})
		transition.to(text, {delay = 300, time = 300, alpha = 1})
	end
	
	if isOnline then
		if _G.storeLoaded == false then
			setupStore()
			native.setActivityIndicator(true)
			_G.storeLoaded = true
			
			if ( system.getInfo( "environment" ) == "simulator" ) then
				showIAPList()
			end
			
		else
			timer.performWithDelay(1000,showIAPList)
		end
	else
		timer.performWithDelay(1500, showConnection, 1 )
	end
end


-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
    elseif ( phase == "did" ) then
		parent = event.parent
        -- Code here runs when the scene is entirely on screen
    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
    elseif ( phase == "did" ) then
	
		parent:updateCoins()
		parent:showAds()
		
		composer.removeScene( "_shop" )
		_G.shopOpen = false
		
		if _G.saleShopBool then
			if timerCd ~= nil then
				timer.cancel(timerCd)
			end
		end
		
        -- Code here runs immediately after the scene goes entirely off screen
    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
	print("BABYE")
    -- Code here runs prior to the removal of scene's view

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene