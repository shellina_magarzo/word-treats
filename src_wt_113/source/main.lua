--[[

version 1.1.1 (modified by Erick - April 3, 2018)
[x] Fix Facebook v4a init
[x] Removed testNetworkConnection() on _shop.lua (function is global anyway)
[] Fix IAP sale event
[x] Fix double proceedwithPurhcase() calls
[x] Coins bought during sale isn't given to the player
[x] Updated app icons (from experiment)
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 1.1.2
[x] Included Corona fix for audio to reduce ANRs
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 1.1.3
[x] Fixed PILLBOX puzzle
[x] Upgraded to Corona build 3276 (hopefully the crashes/ANRs are minimized!)
[x] Fixed more puzzle games
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 1.1.4
* roulette wheel daily reward
	- random rewarding
	- coins price update and effect animations
	- respin for 10 coins feature
	- daily timer for the roulette wheel with some anti cheating checker
	- roulette spin animation: windup on start and bounce back on end
	- level end + coin sound effect on earning coins on the roulette  
	- tick sound effect while spinning the roulette
	- lose sound effect when landing on the "too bad" slice
	- daily roulette now appears the day after first opening the app 
	- pointer shakes on tick sound effect
	- roulette spins 4 times before stopping

~FIXES FROM WORD CUISINE~
- anchor build number to bottom (and make it black)
- claim button hide when not claimable
- sale icon emphasis bug
- disable pause and shop buttons on:
	 - opening extra words
	 - end level screen
	 - pause screen
- iphone layout
- ipad layout
- readjusted buttons size to match game's
- dish and letters placement (anchored to top rather than on the center)
- extra words popup progressbar is hidden when the value is zero
- extra word letters effect
- extra words shake fix
- when extra word is entered again, the extra word button shakes and plays the duplicate word sound
- bug wherein the extra words button doesn't resume shaking when quitting then reentering a level (when not claimed)
- go to next level after level end
- rate us popup
- interstitial ads counting
- locked vertical scroll of shop
- "restore purchases" button on shop hidden on android devices
- android navigation toolbar fix
- fb group popup
- ad banner removed when on main menu and shop
- local notifications
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 1.1.5
- updated rate us popup condition to 10 runs
- updated app icons
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 1.1.6
- watch ad to respin daily roulette when ad is available, costs 10 coins when ad is not available
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
version 1.1.7
- updated puzzles.lua
]]--

--al = nil; if audio2 then audio=audio2; print("=====>>   Using audio2   <<=====") end

local launchArgs = ...

admob = require( "plugin.admob" )
whichPlatform = system.getInfo( "platformName" )    --> Used to check for platforms
gpgs = nil
gameCenter = nil

local composer = require "composer"
local facebook = require "plugin.facebook.v4a"
local notifications = require( "plugin.notifications.v2" )

firebaseAnalytics = require "plugin.firebaseAnalytics"
widget = require( "widget" ) --> Needed to support retina text

require "settings"
json = require( "json" )

targetAppStore = system.getInfo( "targetAppStore" )
if ( "apple" == targetAppStore ) then  -- iOS
    store = require( "store" )
elseif ( "google" == targetAppStore ) then  -- Android
    store = require( "plugin.google.iap.v3" )
elseif ( "amazon" == targetAppStore ) then  -- Amazon
    store = require( "plugin.amazon.iap" )
else
    print( "In-app purchases are not available for this platform." )
end

--system.deactivate ("multitouch")

local function facebookListener( event )
    if ( "fbinit" == event.name ) then
    	print( "Facebook initialized" )
    	facebook.publishInstall()
    elseif ( "fbconnect" == event.name ) then
        if ( "session" == event.type ) then
            -- Handle login event and try to share the link again if needed
        elseif ( "dialog" == event.type ) then
            -- Handle dialog event
        end
    end
end

facebook.init( facebookListener ) --> Init Facebook
firebaseAnalytics.init() --> Init Firebase 

-- Set Defaults Here
display.setDefault( "isAnchorClamped", false )
system.activate( "multitouch" )

gameTitle = "Word Treats"
buildNumber = "1.1.7"
lastScene = ""
--keystore password : p0psicleg@me$
loggedIntoGC = false
isOnline = false

-- used to move the back buttons lower as to not be hidden by the speaker of the Iphone X
onIphoneX = ( string.find( system.getInfo("architectureInfo"), "iPhone10,3" ) ~= nil ) or
                  ( string.find( system.getInfo("architectureInfo"), "iPhone10,6" ) ~= nil )

--[[ code above doesnt work on simulator so set it to true for now.
	DONT FORGET TO COMMENT CODE BELOW WHEN READY TO BUILD --]]
--onIphoneX = ( string.find( system.getInfo("platform"), "ios" ) ~= nil )

--> Init Google Play Services
-- if ( not amazonBuild ) then
	-- print("NOT AMAZON")
	
	-- if ( whichPlatform == "Android") then
		-- gpgs = require( "plugin.gpgs" )
	-- elseif ( whichPlatform == "iPhone OS") then
		-- gameCenter = require( "gameNetwork" )
	-- end
	
-- end

function testNetworkConnection()
    local socket = require("socket")
    local test = socket.tcp()
    test:settimeout( 1000 )  -- Set timeout to 1 second
    local netConn = test:connect( "www.google.com", 80 )
    if ( netConn == nil ) then
        return false
    end
    test:close()
    return true
end

-- Google Play Games Services initialization/login listener
local function gpgsInitListener( event )
	print(">>>>>>>>>>>>>>>>>>> INIT INIT'D")
    if not event.isError then
		print(">>>>>>>>>>>>>>>>>>> NOT ERROR EVENT NAME IS " .. event.name)
        if ( event.name == "init" ) then  -- Initialization event
            -- Attempt to log in the user
			print(">>>>>>>>>>>>>>>>>>> INIT ")
            gpgs.login( { userInitiated=true, listener=gpgsInitListener } )
        elseif ( event.name == "login" ) then  -- Successful login event
            print( json.prettify(event) )
			print("Has Logged In")
			loggedIntoGC = true
        end
	else
		print("ERROR: " .. event.errorMessage .. " " .. event.errorCode)
    end
end

-- Apple Game Center initialization/login listener
local function gcInitListener( event )
    if event.data then  -- Successful login event
        print( json.prettify(event) )
    end
end

-- if gpgs then
	-- Initialize game network based on platform
	-- if isOnline then
		-- Initialize Google Play Games Services
		-- gpgs.init( gpgsInitListener )
		-- print(">>>>>>>>>>>>>>>>>>> GPGS")
	-- elseif ( whichPlatform == "iPhone OS" and isOnline) then
		-- Initialize Apple Game Center
		-- gameCenter.init( "gamecenter", gcInitListener )
		-- print("GC")
	-- end
-- end

--> ADMOB Initialization
function admobListener( event )
    if ( event.phase == "init" ) then  -- Successful initialization
        print( event.provider )
        print( "PRELOADING ADS..." )
        if ( whichPlatform == "iPhone OS" ) then
            admob.load( "interstitial", { adUnitId = "ca-app-pub-8768005339469302/4590552483" } )
			admob.load( "banner", { adUnitId = "ca-app-pub-8768005339469302/1533832660" } )
			admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-8768005339469302/2465390200" } )
        elseif ( whichPlatform == "Android" ) then
            admob.load( "interstitial", { adUnitId = "ca-app-pub-8768005339469302/6422331291" } )
			admob.load( "banner", { adUnitId = "ca-app-pub-8768005339469302/8235628922" } )
			admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-8768005339469302/6249432983" } )
        end
    elseif ( event.phase == "reward" ) then --> Video ad successfully completed
		if ( whichPlatform == "iPhone OS" ) then
			admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-8768005339469302/2465390200" } )
        elseif ( whichPlatform == "Android" ) then
			admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-8768005339469302/6249432983" } )
        end
		rewardAdBool = false
		rewardVideoComplete = true
		settings.set('rewardAdBool', rewardAdBool)
    elseif ( event.phase == "closed" ) then --> Interstitial ad successful and closed
        print( "Ad closed!" )
        print( "RELOADING ADS..." )  --> Refresh banner ad
		if (whichPlatform == "iPhone OS" ) then
			admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-8768005339469302/2465390200" } )
       		admob.load( "interstitial", { adUnitId = "ca-app-pub-8768005339469302/4590552483" } )
        elseif (whichPlatform == "Android" ) then
			admob.load( "rewardedVideo", { adUnitId = "ca-app-pub-8768005339469302/6249432983" } )
        	admob.load( "interstitial", { adUnitId = "ca-app-pub-8768005339469302/6422331291" } )
        end
		
    end
end

--> Init AdMob, replace these with app-specific AdMob IDs
if ( whichPlatform == "iPhone OS" ) then
    adappID = "ca-app-pub-8768005339469302~2202571604"
elseif ( whichPlatform == "Android" ) then
    adappID = "ca-app-pub-8768005339469302~5172828803"
end

admob.init( admobListener, { appId = adappID, testMode = false } )


--> in game default values
extraWordsVal = 0
tempExVal = 0
bannerHeight = 0
adCounter = 0
coinVal = 0
levelNum = 1
doRateUs = true
rateDone = false

playerRunCount = 0

rewardAdBool = false
saleShopBool = false
checkForSale = false
dateOfSale = nil
shopOpen = false

showFBGroupPopup = true
showDailyReward = false

isInMenu = true

-- An array of words to guess of the current level the player is on
cur_WordsToGuess = {}

audios = {}
mydata = nil
dataLevels = {}
validProducts = {}
invalidProducts = {}

audios.sfx = true
audios.music = true
ads = true
productsLoaded = false
storeLoaded = false

local levelOptions = {levelNum = 1, locked = false, complete = false, soupName = "Tomato Soup", levelName = "Onion"}

T_achievements = 
{
	{achievementID = "CgkI2JGyxtoCEAIQAQ", treatName = "Strawberry Shake", lastIng = "Strawberry", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQAg", treatName = "Chocolate Ice Cream", lastIng = "Chocolate", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQAw", treatName = "Caramel Sundae", lastIng = "Cherry", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQBA", treatName = "Lemon Tart", lastIng = "Lemon", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQBQ", treatName = "Pistachio Ice Cream", lastIng = "Pistachio", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQBg", treatName = "Raspberry Panna Cotta", lastIng = "Raspberry", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQBw", treatName = "Rocky Road Ice Cream", lastIng = "Cocoa Powder", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQCA", treatName = "Blueberry Pie", lastIng = "Blueberries", complete = false, awarded = false},
	{achievementID = "CgkI2JGyxtoCEAIQCQ", treatName = "Carrot Cake", lastIng = "Carrots", complete = false, awarded = false}
}

function loadData()
	if ( mydata == nil ) then 
		mydata = {} 
		initializeData()
		for i = 1, #mydata do
			if settings.get('mydata[' .. i .. ']') ~= nil then
				local tempdata = {}
				tempdata[i] = settings.get('mydata[' .. i .. ']')
				mydata[i].complete = tempdata[i].complete
				mydata[i].locked = tempdata[i].locked
				print ("LOADING LEVEL DATA: " .. i)
			end
		end
	end
	
	for i = 1, #T_achievements do
		if settings.get('achievements[' .. i .. ']') ~= nil then
			local tempdata = {}
			tempdata[i] = settings.get('achievements[' .. i .. ']')
			T_achievements[i] = tempdata[i]
			print ("LOADING ACHIEVEMENTS DATA: " .. i)
		end
	end
	
	dateOfSale = settings.get('dateOfSale')
	if ( dateOfSale == nil) then 
		dateOfSale = {}	
	end
	
	coinVal = settings.get('coins')
	if(coinVal == 0 or coinVal == nil) then coinVal = 0 end
	
	saleShopBool = settings.get('saleShopBool')
	if ( saleShopBool == nil) then
		saleShopBool = false
	end
	
	checkForSale = settings.get('checkForSale')
	if checkForSale == nil then
		checkForSale = false
	end
	
	if checkForSale == false then
		saleShopBool = false
	end
	
	rewardAdBool = settings.get('rewardAdBool')
	if ( rewardAdBool == nil) then rewardAdBool = false end
	
	dataLevels = settings.get('dataLevels')
	if ( dataLevels == nil) then dataLevels = {} table.insert(dataLevels,levelOptions) end

	extraWordsVal = settings.get('extraWordsVal')
	if ( extraWordsVal  == nil or extraWordsVal == 0) then extraWordsVal = 0 end
	
	tempExVal = settings.get('tempExVal')
	if ( tempExVal  == nil or tempExVal == 0) then tempExVal = 0 end
	
	audios.sfx = settings.get('sfx')
	if(audios.sfx == nil) then audios.sfx = true end
	
	audios.music = settings.get('music')
	if(audios.music == nil) then audios.music = true end

	showFBGroupPopup = settings.get('fb group')
	if(showFBGroupPopup == nil) then showFBGroupPopup = true end

	showDailyReward = settings.get('daily reward')
	if(showDailyReward == nil) then showDailyReward = false end

	ads = settings.get('ads')
	if(ads == nil) then ads = true end
	
	validProducts = settings.get('validProducts')
	if validProducts == nil then validProducts = nil end
	
	doRateUs = settings.get('doRateUs')
	if ( doRateUs == nil) then doRateUs = false end

    playerRunCount = settings.get('runCount')
    if ( playerRunCount == nil) then playerRunCount = 0 end

	rateDone = settings.get('rateDone')
	if ( rateDone == nil) then rateDone = false end

	cur_WordsToGuess = settings.get('curWTG')
	if(cur_WordsToGuess == nil) then cur_WordsToGuess = {} cur_WordsToGuess[1] = "BE" end
    
    if playerRunCount >= 10 then
        _G.doRateUs = true
        settings.set('doRateUs', _G.doRateUs)
    end

	print("SAVED DATA LOADED SUCCESSFULLY")
end


-- for puzzleData 
local function createData(typeName, productName, soupName, whichPuzzleSet, label, soupIng, locked, complete, lettercount, levelcount) 
	local valType = 0
	local rawData = {}
	if typeName == "header" then
		valType = 1
		rawData = {type = valType}
	elseif typeName == "product" then
		valType = 2
		rawData = {type = valType, label = productName, locked = locked, soupName = soupName}
	elseif typeName == "ingredient" then
		valType = 3
		rawData = {type = valType, label = label, whichPuzzleSet = whichPuzzleSet, locked = locked, 
					complete = complete, lettercount = lettercount, levelcount = levelcount, productName = productName, soupName = soupName, soupIng = soupIng}
	elseif typeName == "footer" then
		valType = 4
		rawData = {type = valType}
	elseif typeName == "other" then
		valType = 5
		rawData = {type = valType}
	end
	table.insert(mydata, rawData)
end

function initializeData ()
	createData("header")
	createData("product", "Strawberry Shake", "Tomato Soup", nil, nil, nil, 0)
	createData("ingredient", "Strawberry Shake", "Tomato Soup", 1, "Milk", "Onion", 0, 0, 3, 5)
	createData("ingredient", "Strawberry Shake", "Tomato Soup", 2, "Strawberry", "Tomato", 1, 0, 3, 10)
	createData("footer")
		
	createData("header")
	createData("product", "Chocolate Ice Cream", "Pumpkin Soup", nil, nil, nil, 1)
	createData("ingredient", "Chocolate Ice Cream", "Pumpkin Soup", 3, "Egg", "Leek", 1, 0, 4, 20)
	createData("ingredient", "Chocolate Ice Cream", "Pumpkin Soup", 4, "White Sugar", "Garlic", 1, 0, 4, 20)
	createData("ingredient", "Chocolate Ice Cream", "Pumpkin Soup", 5, "Milk", "Cream", 1, 0, 4, 20)
	createData("ingredient", "Chocolate Ice Cream", "Pumpkin Soup", 6, "Cream", "Black Pepper", 1, 0, 5, 20)
	createData("ingredient", "Chocolate Ice Cream", "Pumpkin Soup", 7, "Chocolate", "Pumpkin", 1, 0, 5, 20)
	createData("footer")	

	createData("header")
	createData("product", "Caramel Sundae", "Chicken Noodle Soup", nil, nil, nil, 1)
	createData("ingredient", "Caramel Sundae", "Chicken Noodle Soup", 8,  "Pecan", "Celery", 1, 0, 5, 20)
	createData("ingredient", "Caramel Sundae", "Chicken Noodle Soup", 9,  "Butter", "Oregano", 1, 0, 5, 20)
	createData("ingredient", "Caramel Sundae", "Chicken Noodle Soup", 10, "Cream", "Basil", 1, 0, 5, 20)
	createData("ingredient", "Caramel Sundae", "Chicken Noodle Soup", 11, "Caramel", "Egg Noodles", 1, 0, 5, 20)
	createData("ingredient", "Caramel Sundae", "Chicken Noodle Soup", 12, "Cherry", "Chicken Broth", 1, 0, 6, 20)
	createData("footer")

	createData("header")
	createData("product", "Lemon Tart", "Egg Drop Soup", nil, nil, nil, 1)
	createData("ingredient", "Lemon Tart", "Egg Drop Soup", 13, "Flour", "Egg", 1, 0, 5, 20)
	createData("ingredient", "Lemon Tart", "Egg Drop Soup", 14, "Pine Nut", "Cornstarch", 1, 0, 5, 20)
	createData("ingredient", "Lemon Tart", "Egg Drop Soup", 15, "Brown Sugar", "Green Onion", 1, 0, 5, 20)
	createData("ingredient", "Lemon Tart", "Egg Drop Soup", 16, "Egg", "Chicken Broth", 1, 0, 5, 20)
	createData("ingredient", "Lemon Tart", "Egg Drop Soup", 17, "Lemon", "Soy Sauce", 1, 0, 6, 20)
	createData("footer")

	createData("header")
	createData("product", "Pistachio Ice Cream", "Cream of Mushroom", nil, nil, nil, 1)
	createData("ingredient", "Pistachio Ice Cream", "Cream of Mushroom", 18, "Whipped Cream", "Fresh Mushroom", 1, 0, 5, 20)
	createData("ingredient", "Pistachio Ice Cream", "Cream of Mushroom", 19, "Egg Yolk", "Onion", 1, 0, 5, 20)
	createData("ingredient", "Pistachio Ice Cream", "Cream of Mushroom", 20, "Almond Extract", "Garlic", 1, 0, 5, 20)
	createData("ingredient", "Pistachio Ice Cream", "Cream of Mushroom", 21, "White Sugar", "Butter", 1, 0, 5, 20)
	createData("ingredient", "Pistachio Ice Cream", "Cream of Mushroom", 22, "Full Cream Milk", "Evaporated Milk", 1, 0, 6, 20)
	createData("ingredient", "Pistachio Ice Cream", "Cream of Mushroom", 23, "Pistachio", "Nutmeg", 1, 0, 6, 20)
	createData("footer")

	createData("header")
	createData("product", "Raspberry Panna Cotta", "Clam Chowder", nil, nil, nil, 1)
	createData("ingredient", "Raspberry Panna Cotta", "Clam Chowder", 24, "Honey", "Cream", 1, 0, 6, 20)
	createData("ingredient", "Raspberry Panna Cotta", "Clam Chowder", 25, "White Sugar", "Minced Clams", 1, 0, 6, 20)
	createData("ingredient", "Raspberry Panna Cotta", "Clam Chowder", 26, "Gelatin", "Onion", 1, 0, 7, 20)
	createData("ingredient", "Raspberry Panna Cotta", "Clam Chowder", 27, "Mint", "Celery", 1, 0, 7, 20)
	createData("ingredient", "Raspberry Panna Cotta", "Clam Chowder", 28, "Cream", "Potato", 1, 0, 7, 20)
	createData("ingredient", "Raspberry Panna Cotta", "Clam Chowder", 29, "Vanilla", "Carrot", 1, 0, 7, 20)
	createData("ingredient", "Raspberry Panna Cotta", "Clam Chowder", 30, "Raspberry", "Butter", 1, 0, 7, 20)
	createData("footer")

	createData("header")
	createData("product", "Rocky Road Ice Cream", "Tortilla Soup", nil, nil, nil, 1)
	createData("ingredient", "Rocky Road Ice Cream", "Tortilla Soup", 31, "Egg Yolk", "Tortilla", 1, 0, 7, 20)
	createData("ingredient", "Rocky Road Ice Cream", "Tortilla Soup", 32, "Walnut", "Jalapeño", 1, 0, 7, 20)
	createData("ingredient", "Rocky Road Ice Cream", "Tortilla Soup", 33, "Sweet Chocolate", "Cilantro", 1, 0, 7, 20)
	createData("ingredient", "Rocky Road Ice Cream", "Tortilla Soup", 34, "Vanilla", "Bell Pepper", 1, 0, 7, 20)
	createData("ingredient", "Rocky Road Ice Cream", "Tortilla Soup", 35, "Marshmallow", "Ground Beef", 1, 0, 7, 20)
	createData("ingredient", "Rocky Road Ice Cream", "Tortilla Soup", 36, "Whole Milk", "Cheddar Cheese", 1, 0, 7, 20)
	createData("ingredient", "Rocky Road Ice Cream", "Tortilla Soup", 37, "Cocoa Powder", "Chipotle Chili", 1, 0, 7, 20)
	createData("footer")
	
	createData("header")
	createData("product", "Blueberry Pie", "Blueberry Pie", nil, nil, nil, 1)
	createData("ingredient", "Blueberry Pie", "Blueberry Pie", 38, "Flour", "Flour", 1, 0, 7, 20)
	createData("ingredient", "Blueberry Pie", "Blueberry Pie", 39, "Shortening", "Shortening", 1, 0, 7, 20)
	createData("ingredient", "Blueberry Pie", "Blueberry Pie", 40, "Butter", "Butter", 1, 0, 7, 20)
	createData("ingredient", "Blueberry Pie", "Blueberry Pie", 41, "Lemon Juice", "Lemon Juice", 1, 0, 7, 20)
	createData("ingredient", "Blueberry Pie", "Blueberry Pie", 42, "Cornstarch", "Cornstarch", 1, 0, 7, 20)
	createData("ingredient", "Blueberry Pie", "Blueberry Pie", 43, "Cinnamon", "Cinnamon", 1, 0, 7, 20)
	createData("ingredient", "Blueberry Pie", "Blueberry Pie", 44, "Blueberries", "Blueberries", 1, 0, 7, 20)
	createData("footer")
	
	createData("header")
	createData("product", "Carrot Cake", "Carrot Cake", nil, nil, nil, 1)
	createData("ingredient", "Carrot Cake", "Carrot Cake", 45, "Icing Sugar", "Icing Sugar", 1, 0, 7, 20)
	createData("ingredient", "Carrot Cake", "Carrot Cake", 46, "Vanilla", "Vanilla", 1, 0, 7, 20)
	createData("ingredient", "Carrot Cake", "Carrot Cake", 47, "Nutmeg", "Nutmeg", 1, 0, 7, 20)
	createData("ingredient", "Carrot Cake", "Carrot Cake", 48, "Raisins", "Raisins", 1, 0, 7, 20)
	createData("ingredient", "Carrot Cake", "Carrot Cake", 49, "Walnuts", "Walnuts", 1, 0, 7, 20)
	createData("ingredient", "Carrot Cake", "Carrot Cake", 50, "Cream Cheese", "Cream Cheese", 1, 0, 7, 20)
	createData("ingredient", "Carrot Cake", "Carrot Cake", 51, "Carrots", "Carrots", 1, 0, 7, 20)
	createData("footer")
	
	
	createData("other")	
end

--Composer Transition
composer.effectList.slideOverRight = {sceneAbove = false,
      concurrent = true ,
      from =  {  yStart = 0 , xStart = 0 , xEnd = display.contentWidth+100, transition = easing.inQuad , yEnd = 0},
      to = {yStart = 0, xStart = 0 , xEnd = 0, alphaStart = 0.1, alphaEnd = 1, transition = easing.inQuad, yEnd = 0 },
    }

composer.effectList.slideOverLeft = {sceneAbove = false,
      concurrent = true ,
      from =  {  yStart = 0 , xStart = 0 , xEnd = -display.contentWidth-100, transition = easing.inQuad , yEnd = 0},
      to = {yStart = 0, xStart = 0 , xEnd = 0, alphaStart = 0.1, alphaEnd = 1, transition = easing.inQuad, yEnd = 0 },
    }
	
--> Used for anchoring objects
anchor = {
    TopLeft = function(t) t.anchorX, t.anchorY = 0, 0; end,
    TopCenter = function(t) t.anchorX, t.anchorY = .5, 0; end,
    TopRight = function(t) t.anchorX, t.anchorY = 1, 0; end,
    CenterLeft = function(t) t.anchorX, t.anchorY = 0, .5; end,
    Center = function(t) t.anchorX, t.anchorY = .5, .5; end,
    CenterRight = function(t) t.anchorX, t.anchorY = 1, .5; end,
    BottomLeft = function(t) t.anchorX, t.anchorY = 0, 1; end,
    BottomCenter = function(t) t.anchorX, t.anchorY = .5, 1; end,
    BottomRight = function(t) t.anchorX, t.anchorY = 1, 1; end,
}

-- Load Image Ceate Image
function newImageRectNoDimensions( filename , loc)

    if loc == nil then loc = system.ResourceDirectory end
    local w, h = 0, 0
    local image
    image = display.newImage( filename, loc )
    w =  math.floor(image.width)
    h =  math.floor(image.height)
    image:removeSelf()

    return display.newImageRect( filename, loc, w, h )
end

-- audio
audio.reserveChannels( 5 )
bgm = audio.loadStream("res/audio/BGM_GunmaGambol.mp3")

function loadSoundTable()
	soundTable = {
		letterSelect = audio.loadSound( "res/audio/LetterSelect.mp3" ),
		letterShuffle = audio.loadSound("res/audio/LetterShuffle.mp3"),
		showHint = audio.loadSound("res/audio/ShowHint.mp3"),
		HidePause = audio.loadSound( "res/audio/HidePause.mp3" ),
		showPause = audio.loadSound( "res/audio/ShowPause.mp3" ),
		luckyBowl = audio.loadSound("res/audio/ShowLuckyBowl.mp3"),
		claimGold = audio.loadSound( "res/audio/GoldClaim.mp3" ),
		popUp = audio.loadSound("res/audio/Pop-ups.mp3"),
		wordFound = audio.loadSound("res/audio/FoundWord.mp3"),
		extraWord = audio.loadSound("res/audio/ExtraWord.mp3"),
		noWord = audio.loadSound("res/audio/WrongWord.mp3"),
		dupWord = audio.loadSound("res/audio/RepeatWord.mp3"),
		coinBomb = audio.loadSound("res/audio/CoinBomb.mp3"),
		levelEnd = audio.loadSound("res/audio/LevelEnd.mp3"),
		lose = audio.loadSound("res/audio/lose.mp3"),
		tick = audio.loadSound("res/audio/tick.mp3")
	}	
end
function audioButtonSelect()
	buttonSelect = audio.loadSound("res/audio/ButtonSelect.mp3")
	audio.play(buttonSelect, {channel = 2})
end

if audios.sfx then
	for i = 2,32 do
		audio.setVolume(0.5, {channel=i})
	end	
else
	for i = 2,32 do
		audio.setVolume(0, {channel=i})
	end	
end

audio.play(bgm, { channel=1, loops=-1,  } )
if audios.music then
	audio.setVolume( 1, { channel=1 } )
else
	audio.setVolume( 0, { channel=1 } )
end

--Change a block color
function randomFillColor(obj)
	local R = tonumber("."..math.random(0,9))
	local G = tonumber("."..math.random(0,9))
	local B = tonumber("."..math.random(0,9))
	obj:setFillColor(R,G,B)
end

--Delta Time
local runtime = 0
 
function getDeltaTime()
    local temp = system.getTimer()  -- Get current game time in ms
    local dt = (temp-runtime) / (1000/60)  -- 60 fps or 30 fps as base
    runtime = temp  -- game time
    return dt
end

function hideToolBar()
	if system.getInfo("platformName") == "Android" then
	    if system.getInfo("androidApiLevel") >= 23 then
			native.setProperty( "androidSystemUiVisibility", "immersive" )
	        native.setProperty( "androidSystemUiVisibility", "immersiveSticky" )
	    end
	end
end

function hideAdBanner( )
	_G.admob.hide()
	print ("BANNER: Hiding Banner Ad")
	_G.bannerVisible = false
end

display.setStatusBar( display.HiddenStatusBar )

--For Android 6, hide bottom bar
if system.getInfo("platformName") == "Android" then
    if system.getInfo("androidApiLevel") >= 23 then
    	native.setProperty( "androidSystemUiVisibility", "immersive" )
        native.setProperty( "androidSystemUiVisibility", "immersiveSticky" )

    end
end

function checkMemory()
   collectgarbage( "collect" )
   local memUsage_str = string.format( "MEMORY = %.3f KB", collectgarbage( "count" ) )
   print( memUsage_str)--, "TEXTURE = "..(system.getInfo("textureMemoryUsed") / (1024 * 1024) ) )
end
--timer.performWithDelay( 1000, checkMemory, 0 )  --uncomment to determine memory usage

bannerVisible = false
rewardVideoComplete = false

local function onNotification( event )
	if event.type == "remote" then
	elseif event.type == "local" then
		native.setProperty( "applicationIconBadgeNumber", 0 ) -- resets notification badge when app is opened from notif
	end
end

local dateToday
local pattern = "(%d+)%-(%d+)%-(%d+)%/(%d+):(%d+):(%d+)"
local dyear, dmonth, dday, dhour, dmin, dsec 
local xyear, xmonth, xday, xhour, xmin, xsec

cday = 0
chour = 0
cmin = 0
csec = 0

-- Checking for SALE and TIME ELAPSED AFTER SALE INITIATION
function checkTime()
	if checkForSale then
		print("---------------------------------")
		print("CHECKING TIME ....")
		local thisDate = os.date("*t")
		dateToday = tostring(os.date("%Y-%m-%d/%H:%M:%S", os.time(t)))
		print("---------------------------------")
		print("DATE TODAY: " .. dateToday)
		dyear, dmonth, dday, dhour, dmin, dsec = dateToday:match(pattern)
	
		if dateOfSale.sT ~= nil then
			print("START SALE TIME: " .. dateOfSale.sT)
			print("---------------------------------")
		
			saleShopBool = false
			
			xyear, xmonth, xday, xhour, xmin, xsec = dateOfSale.sT:match(pattern)
			
			local function compareHour()
				if xhour == dhour then
					if dmin > xmin then
						saleShopBool = true
					elseif dmin == xmin then
						if dsec >= xsec then
							saleShopBool = true
						end
					else
						saleShopBool = false
					end	
				else
					saleShopBool = true
				end
			end
			
			if xyear == dyear then
				if xmonth == dmonth and dday >= xday then				
					if dday == xday and dhour >= xhour then
						saleShopBool = true
						compareHour()
					end
					if (dday+0) == (xday+1) and xhour >= dhour then
						if xhour == dhour then
							if dmin > xmin then
								saleShopBool = false
							else
								saleShopBool = true
							end	
						elseif xhour > dhour then
							saleShopBool = true
						else
							saleShopBool = false
						end
					end
				elseif dmonth > xmonth and dday == "01" then
					if xhour >= dhour then
						if xhour == dhour then
							if xmin > dmin then
								saleShopBool = true
							elseif dmin == xmin then
								if xsec >= dsec then
									saleShopBool = true
								end
							else
								saleShopBool = false
							end	
						elseif xhour > dhour then
							saleShopBool = true
						end
					end
				end
			end
			
			
			local function compareSec()
				if xsec > dsec then
					csec = 60 - ( xsec - dsec )
					cmin = cmin - 1
				elseif dsec > xsec then
					csec = dsec - xsec
				else
					csec = 0
				end
			end
			
			if saleShopBool then
			-- TIME ELAPSED COMPUTATION [EDIT AT YOUR OWN RISK]
				if dhour == xhour and dday == xday then -- same day
					if dmin > xmin then
						if dsec > xsec then
							cmin = dmin - xmin
							csec = dsec - xsec
						elseif xsec > dsec then
							cmin = dmin - xmin
							cmin = cmin - 1
							
							csec = 60 - ( xsec - dsec )
						else
							csec = 0
						end
					end
				elseif dhour > xhour and dday == xday then -- same day
					chour = dhour - xhour
					if xmin > dmin then
						cmin = xmin - dmin
						compareSec()
					elseif xmin < dmin then
						cmin = dmin - xmin
						compareSec()
					else
						if xsec > dsec then
							csec = xsec - dsec
							cmin = 59
							chour = chour - 1
						elseif xsec < dsec then
							csec = dsec - xsec
						else
							csec = 0
						end
					end
				elseif (dday > xday and dmonth == xmonth) or (xday > dday and dmonth > xmonth) then -- next day
					chour = (24 - xhour) + dhour
					if xhour > dhour then
						if xmin > dmin then
							cmin = xmin - dmin
							compareSec()
						elseif xmin < dmin then
							cmin = dmin - xmin
							compareSec()
						else
							if xsec > dsec then
								csec = xsec - dsec
								cmin = 59
								chour = chour - 1
							elseif xsec < dsec then
								csec = dsec - xsec
							else
								csec = 0
							end
						end
					else -- same hour
						chour = 22
						if xmin > dmin then
							cmin = 60 - (xmin - dmin)
							compareSec()
							print("CHET")
						elseif xmin < dmin then
							cmin = dmin - xmin
							compareSec()
						else
							cmin = 59
							if xsec > dsec then
								csec = 60 - ( xsec - dsec )
							else
								_G.saleShopBool = false
							end
						end
					end
				end
				
				print("HOUR/S PASSED: " .. chour)
				print("MINS PASSED: " .. cmin)
				print("SEC PASSED: " .. csec)
			end
		end
	end
end

local function checkingSale()
	checkForSale = settings.get('checkForSale')
	if checkForSale == false or checkForSale == nil then
		print("<<<<<< CHECK FOR SALE BOOL IS FALSE/NIL. CHECKING FOR SALE")
		local sendNotif = false
		for i = 1, #mydata do 	
			if mydata[i].type == 3 and mydata[i].label == "Cream" and mydata[i].productName == "Caramel Sundae" and mydata[i].locked == 0 and coinVal <= 100 then
				checkForSale = true
				settings.set('checkForSale', checkForSale)
				
				local t = os.date('*t')
				local u = t
				u.hour = u.hour + 1
				local uString = tostring(os.date("%Y-%m-%d/%H:%M:%S", os.time(u)))
				print("<<<<<< START TIME OF SALE: " .. uString)

				dateOfSale.sT = uString
				settings.set('dateOfSale', _G.dateOfSale)
				
				sendNotif = true
			end
		end
		
		if sendNotif then
			local notification_options = {
			   alert = "[24HR 50% SALE] Need more coins? Check the SHOP now!",
			   badge = 1
			}
			--> Schedule a local notification x seconds after quitting the app
			local notification = notifications.scheduleNotification( 3600, notification_options )
			
			local notification2_options = {
			   alert = "[LAST CHANCE] Only 1 hour remaining of our special offer! What are you waiting for?",
			   badge = 1
			}
			--> Schedule a local notification x seconds after quitting the app
			local notification2 = notifications.scheduleNotification( 82800, notification2_options )
			
			
			print("<<<<<< SCHEDULING NOTIFICATIONS")
			
			
		end
		
	end
end

-- Returns the a table of info about the current save the player is on
function getCurrentTable()
	-- Contains: thisMenu, treatName, thisIngredient, treatIngName, thisLC, thisLevelCount, thisLevel
	local current = {}

	for i = 1, #mydata do
		if mydata[i].type == 2 and mydata[i].locked == 0 then
			current.thisMenu = mydata[i].soupName
			current.treatName = mydata[i].label
		end
	end
	
	for i = 1, #mydata do
		if mydata[i].type == 3 and mydata[i].soupName == current.thisMenu and mydata[i].locked == 0 then
			current.thisIngredient = mydata[i].soupIng
			current.treatIngName = mydata[i].label
			current.thisIndex = i
			current.thisLC = mydata[i].lettercount
			current.thisLevelCount = mydata[i].levelcount
		end
	end

	local indexData
	for i = 1, #dataLevels do
		if dataLevels[i].soupName == current.thisMenu and dataLevels[i].levelName == current.thisIngredient then
			current.thisLevel = dataLevels[i].levelNum 
			indexData = i
			if dataLevels[indexData].complete == true and dataLevels[indexData].levelNum + 1 <= thisLevelCount then
				current.thisLevel = dataLevels[indexData].levelNum + 1
			end
		end
	end

	return current
end

-- Returns a randomly picked unsubmitted answer from the current level the player is on
local function getHintWord()

	local length = 0 -- the length of the shortest word
	local hint_words = {} -- the table the contains the shortest words

	-- Find the length of the shortest word
	for i=1, getCurrentTable().thisLC do
		if (length ~= 0 ) then break end

		for j=1, #cur_WordsToGuess do
			if (string.len(cur_WordsToGuess[j]) == i) then length = i break end
		end
	end

	-- Make a table of words only with the shortest length
	for i = 1, #cur_WordsToGuess do
		print( cur_WordsToGuess[i] )
		if ( (string.len(cur_WordsToGuess[i]) == length) ) then
			table.insert( hint_words, cur_WordsToGuess[i] )
		end
	end

	local random
	if #hint_words ~= 0 then 
		random = math.random( #hint_words ) 
	else random = 0 end

	return hint_words[random]
end


local notif_hint, notif_roulette

local function setReminderNotif()
	local thisDate = os.date("*t")
	local dyear, dmonth, dday, dhour, dmin, dsec 
	local notifTime

	local hintWord = getHintWord()
	local curDish = getCurrentTable().treatName
	local hintPhrase, roulettePhrase

	-- Set this one. The hour (in military time) that the notification will show daily
	local notifHour = 19

	dateToday = tostring(os.date("%Y-%m-%d/%H:%M:%S", os.time(t)))
	dyear, dmonth, dday, dhour, dmin, dsec = dateToday:match(pattern)

	local diff_hour, diff_min

	-- Math for calculating time to show the notification
	if (tonumber(dhour) >= notifHour) then
		diff_hour = (23 - dhour) + 24 + notifHour
	else
		diff_hour = (23 - dhour) + notifHour
	end

	diff_min = 60 - dmin

	-- Convert the hours and minutes to seconds
	notifTime = (diff_hour * 60 * 60) + (diff_min * 60)

	-- Pick the phrase for the hint notification
	local rand = math.random( 4 )

	if hintWord and curDish then
		if (rand == 1) then
			hintPhrase = "Try " .. hintWord .. "! That will probably work."
		elseif (rand == 2) then
			hintPhrase = "Need a hand? Try " .. hintWord .. "!"
		elseif (rand == 3) then
			hintPhrase = "Try " .. hintWord .. " to enjoy more " .. curDish .. "."
		else
			hintPhrase = "Stuck? " .. hintWord .. " is one of the words."
		end
	else
		hintPhrase = "Play and satisfy your sweet tooth for words!"
	end

	-- Pick the phrase for the daily roulette notification
	local rand2 = math.random( 3 )
	if (rand2 == 1) then
		roulettePhrase = "Come back! You have a free spin!"
	elseif (rand2 == 2) then
		roulettePhrase = "Feeling lucky? Spin and earn free coins!"
	else
		roulettePhrase = "Get free coins! Spin the wheel!"
	end

	-- Set up notification options
	local notifHint_options = {
	    alert = hintPhrase,
	    badge = 1
	}
	local notifRoulette_options = {
	    alert = roulettePhrase,
	    badge = 2
	}

	-- Schedule the notification for the first day
	notif_hint = notifications.scheduleNotification(notifTime, notifHint_options)
	
	-- Schedule the notification for the second day	
	notif_roulette = notifications.scheduleNotification( (notifTime + 86400), notifRoulette_options)

	print( ">>>>>>>>> DAILY NOTIFICATIONS SCHEDULED <<<<<<<<<<<<<" )
end

-- Cancel all the reminder notifications when game is resumed
local function cancelNotifications( loc )
	print("cancelled notifs " .. loc)
	notifications.cancelNotification( )
end

function onSystemEvent( event ) 
	if event.type == "applicationStart" then
		cancelNotifications("start")
        playerRunCount = playerRunCount + 1
        settings.set('runCount', playerRunCount)

        print(playerRunCount)

		return true
	elseif event.type == "applicationResume" then
		if _G.shopOpen and checkForSale then
			print("REFRESH")
			checkTime()
			
			if _G.saleShopBool then 
				native.setActivityIndicator(true)
				timer.performWithDelay(1000, function() composer.showOverlay( "_shop", {time = 500,  isModal = true } ) end )
				composer.hideOverlay("fade", 500 )
			end
		end
		cancelNotifications("resume")
		native.setProperty( "applicationIconBadgeNumber", 0 ) -- resets notification when app is opened
		return true
	elseif event.type == "applicationSuspend" then
		suspendTime = os.time()
		checkingSale()
		setReminderNotif()
		return true
	elseif event.type == "applicationExit" then
		return true
	end
end

if ( launchArgs and launchArgs.notification ) then
	onNotification( launchArgs.notification )
end

Runtime:addEventListener( "notification", onNotification )
native.setProperty( "applicationIconBadgeNumber", 0 ) --> resets notification when app is opened
Runtime:addEventListener( "system", onSystemEvent )

print("HEIGHT: " .. display.contentHeight)
print("WIDTH: " .. display.contentWidth)

local fonts = native.getFontNames()
local count = 0

for i,fontname in ipairs(fonts) do -- Count the number of total fonts
    count = count+1
end

print( "FONT COUNT: " .. count )
local name = "KIMBERLEY"     -- part of the Font name we are looking for
name = string.lower( name )

for i, fontname in ipairs(fonts) do -- Display each font in the terminal console
    j, k = string.find( string.lower( fontname ), name )
    if( j ~= nil ) then
        print( "fontname = " .. tostring( fontname ) )
    end
end

local function main()
	settings.load() --> Read JSON file
	loadData() --> Load all saved data
	checkTime()
end 

main()

composer.gotoScene( "_mainMenu", "fade", 500)
