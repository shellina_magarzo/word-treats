
-- -----------------------------------------------------------------------------------
-- Main Menu.lua
-- -----------------------------------------------------------------------------------

local composer = require( "composer" )
local globals = require( "globals" )
local widget = require( "widget" )
local inspect = require( "inspect" )
local displayGroups = {}
local json = require( "json" )

local scene = composer.newScene()
local logo
	
local thisMenu
local thisIngredient
local thisLevel = 1
local thisIndex
local thisLC

local treatName
local treatIngName

local checkerBool 
local timerEmphasis

_G.lastScene = "_mainMenu"

local closeFBGPopup

local function handleReleaseEvent( event )
    local buttonID = event.target.id
	
	if event.phase == "began" then
	elseif event.phase == "ended" then
		_G.audioButtonSelect()
		if buttonID == "play" then
			_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "start_game", item_id = "1"} )
			
			if #_G.dataLevels >= 1 and _G.dataLevels[1].complete then
				composer.setVariable( "menuName", thisMenu)	
				composer.setVariable( "treatName", treatName)
				composer.setVariable( "treatIngName", treatIngName)
				composer.setVariable( "levelName", thisIngredient)	
				composer.setVariable( "levelNum", thisLevel)
				composer.setVariable( "letterCount", thisLC)
				composer.setVariable( "dataIndex", thisIndex)
				composer.setVariable( "levelCount", thisLevelCount)
				composer.gotoScene( "_game", { effect = "fade", time = 300 } )
			else
				composer.gotoScene( "menu", { effect = "fromBottom", time = 300 } )
			end
		elseif buttonID == "shop" then
			composer.showOverlay( "_shop", { effect = "slideDown", time = 500, isModal = true } )
		elseif buttonID == "credits" then
			composer.showOverlay( "credits", { effect = "slideDown", time = 500, isModal = true } )	
		elseif buttonID == "noAds" then
			composer.showOverlay( "_shop", { effect = "slideDown", time = 500, isModal = true } )
		elseif buttonID == "likeUs" then
			system.openURL("https://www.facebook.com/popsiclegames/")
		elseif buttonID == "fb group" then
			system.openURL("https://www.facebook.com/groups/wordgamelovers/")
			_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "join_fb_group", item_id = "1"} )
			showFBGroupPopup = false
			settings.set('fb group', showFBGroupPopup)	
			closeFBPopup()
		end
	end
end

-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
local function showDailyRewardPopup( delay )
	if showDailyReward then
		timer.performWithDelay(delay, function (  )
						composer.showOverlay( "popUp_wheel", { effect = "fade", time = 500,  isModal = true } )  
					end)
	end
end

-- Update function
local frameCount = 0

-- For the tick sound during the spin
function update(event)
	if ( (frameCount % 30) == 0) then
		hideToolBar()
	end
    frameCount = frameCount + 1
end
 
Runtime:addEventListener("enterFrame", update) 

-- create()
function scene:create( event )

    local sceneGroup = self.view
	font = "res/fonts/BalooTammudu-Regular.ttf"
	
    local backGroup = globals.createDisplayGroup( "backGroup", displayGroups, sceneGroup )
    local mainGroup = globals.createDisplayGroup( "mainGroup", displayGroups, sceneGroup )
	local pauseGroup = globals.createDisplayGroup( "pauseGroup", displayGroups, sceneGroup )
	local fbGroup = globals.createDisplayGroup( "fbGroup", displayGroups, sceneGroup )
	
	local optionsBtn 
	local playBtn
	local shopBtn
	local rectOverlay	
	local closeBtn
	local noAdsBtn

	_G.isInMenu = true
	hideAdBanner()
	
	local function openOptions()
		_G.audioButtonSelect()
		rectOverlay.isVisible = true
		pauseGroup.isVisible = true
		pauseGroup.y = -pauseGroup.height
		optionsBtn:setEnabled(false)
		shopBtn:setEnabled(false)
		playBtn:setEnabled(false)
		transition.to(rectOverlay, {time = 400, alpha = 0.8})
		transition.to(pauseGroup, {time = 500, y = -20, transition = easing.inOutExpo})
		_G.firebaseAnalytics.logEvent( "SELECT_CONTENT", { content_type = "viewed_settings", item_id = "1"} )
	end
	
	local function closeOptions()
		_G.audioButtonSelect()
		transition.to(rectOverlay, {time = 400, alpha = 0})
		transition.to(pauseGroup, {time = 700, y = -pauseGroup.height - 100, transition = easing.inOutExpo, 
		onComplete = function() 
			pauseGroup.isVisible = false  
			optionsBtn:setEnabled(true)
			shopBtn:setEnabled(true)
			playBtn:setEnabled(true)
		end})
		
	end
	
	local holidayBool = false
	local bgName
	
	local date = os.date("*t")
	local datetoday = tostring(date.year.."-"..string.format("%02d", date.month).."-"..string.format("%02d", date.day))
	local pattern = "(%d+)%-(%d+)%-(%d+)"
	local xyear, xmonth, xday = datetoday:match(pattern)

	local function checkFBDate( )
		-----------------------------------------
		-- For Facebook group popup 
		-----------------------------------------
		local day_stored = 0
		local month_stored = 0
		
		day_stored = settings.get('day_stored')
		if(day_stored == nil) then day_stored = xday end
		
		month_stored = settings.get('month_stored')
		if(month_stored == nil) then month_stored = xmonth end
		
		--[[
		print ("Stored Month: " .. month_stored )
		print ("Stored Day: " .. day_stored)--]]
		
		if (month_stored == xmonth) then
			if (tonumber(xday) >= (tonumber(day_stored)+ 2)) then
				showFBGroupPopup = true
			end
		else
			showFBGroupPopup = true
		end

		settings.set('fb group', showFBGroupPopup)
		-----------------------------------------
	end

	-----------------------------------------
	-- For Show Daily Reward
	-----------------------------------------
	local function checkDailyRewardDate(  )
		local day_stored = 0
		local month_stored = 0
		local year_stored = 0

		day_stored = settings.get('day_stored_reward')
		if(day_stored == nil) then day_stored = xday end

		month_stored = settings.get('month_stored_reward')
		if(month_stored == nil) then month_stored = xmonth end

		year_stored = settings.get('year_stored_reward')
		if(year_stored == nil) then year_stored = xyear end

		--[[
		print ("Stored Month: " .. month_stored )
		print ("Stored Day: " .. day_stored)
		print ("Stored Year: " .. year_stored) --]]

		--[[
		Checks the stored date and today's date to check if a day has passed
		Has some anti-cheat to prevent player's from changing the date on their phone
		However, it has some limitations. It works by disabling daily rewards when the current date is before the stored date, but never after
		This can be abused by having the players constantly incrementing the date then getting the daily reward, but not going back
		Todo: verify current day by usage of internet time
		PS: Not used for FB group popup because ... more exposure? lol c: // https://stackoverflow.com/questions/19425858/how-to-pass-variable-by-reference-in-corona //--]] 
		if (xyear == year_stored) then
			if (xmonth == month_stored) then
				if (xday > day_stored) then
					showDailyReward = true
				else
					showDailyReward = false
				end
			elseif (xmonth > month_stored) then
				showDailyReward = true
			else
				showDailyReward = false
			end
		elseif (xyear > year_stored) then
			showDailyReward = true
		elseif (xyear < year_stored) then
			showDailyReward = false
		end

		-- If there is no stored day data yet (which means this is the first opening of the app), don't show the daily reward
		if settings.get('day_stored_reward') == nil then 
			showDailyReward = false
			settings.set('day_stored_reward', xday)
			settings.set('month_stored_reward', xmonth)
		end

		settings.set('daily reward', showDailyReward)
	end
	-----------------------------------------

	checkFBDate()
	checkDailyRewardDate()


	local function openFBPopup()
		fbRectOverlay.isVisible = true
		fbGroup.isVisible = true
		fbGroup.y = -fbGroup.height
		optionsBtn:setEnabled(false)
		shopBtn:setEnabled(false)
		playBtn:setEnabled(false)
		noAdsBtn:setEnabled(false)
		transition.to(fbRectOverlay, {time = 400, alpha = 0.8})
		transition.to(fbGroup, {time = 500, y = -20, transition = easing.inOutExpo})
	end
	
	function closeFBPopup()
		_G.audioButtonSelect()
		showFBGroupPopup = false
		settings.set('fb group', showFBGroupPopup)
		
		day_stored = xday
		month_stored = xmonth
		
		settings.set('day_stored', day_stored)
		settings.set('month_stored', month_stored)

		showDailyRewardPopup( 30 )
		
		transition.to(fbRectOverlay, {time = 400, alpha = 0})
		transition.to(fbGroup, {time = 700, y = -fbGroup.height - 100, transition = easing.inOutExpo, 
		onComplete = function() 
			fbGroup.isVisible = false  
			optionsBtn:setEnabled(true)
			shopBtn:setEnabled(true)
			playBtn:setEnabled(true)
			noAdsBtn:setEnabled(true)
		end})
	end
	
	if xmonth == "12" or ( xmonth == "01" and xday <= "15") then -- CHRISTMAS
		holidayBool = true
		
		bgName = "bgTitleScreen.png"
		print("<<<<<<<< LOADING CHRISTMAS BG")
	elseif xmonth == "02" and (xday >= "15" and xday <= "29" ) then -- CHINESE NEW YEAR (UPDATE THIS EVERY YEAR)
		holidayBool = true
		
		bgName = "bgTitleScreenCNY.png"
		print("<<<<<<<< LOADING CHINESE NEW YEAR BG")
	end
	
	if not holidayBool then
		bgName = "bgTitleScreen.png"
		print("<<<<<<<< LOADING DEFAULT BG")
	end
	
	local bgBottom = newImageRectNoDimensions( "res/graphics/menu/" .. bgName)
	-- Commented next 2 lines to prevent warping of title screen
	--bgBottom.width = display.actualContentWidth
	--bgBottom.height = display.actualContentHeight
    --globals.setBetterAnchor( bgBottom )
    bgBottom.x = display.contentCenterX
    bgBottom.y = display.contentCenterY
    mainGroup:insert( bgBottom )

    logo = newImageRectNoDimensions( "res/graphics/menu/logo1.png" )
    logo.x = display.contentCenterX
    logo.y = 10
	logo.anchorY = 0
    mainGroup:insert( logo )
	
	playBtn = globals.createImageButton( "play",
					display.contentCenterX, logo.y + logo.height + 50,
					"res/graphics/menu/btn_PlayUp.png", "res/graphics/menu/btn_PlayDown.png",
					mainGroup, handleReleaseEvent, 148, 58)
					
	optionsBtn = globals.createImageButton( "options",
					display.contentCenterX - 65, playBtn.y + playBtn.height ,
					"res/graphics/menu/btnOptionsUp.png", "res/graphics/menu/btnOptionsDown.png",
					mainGroup, openOptions, 115, 45)
					
	shopBtn = globals.createImageButton( "shop",
					display.contentCenterX + 65, playBtn.y + playBtn.height,
					"res/graphics/menu/btnShopUp.png", "res/graphics/menu/btnShopDown.png",
					mainGroup, handleReleaseEvent, 115, 45)
	
	if _G.saleShopBool then
		local saleImg = newImageRectNoDimensions("res/graphics/menu/uiSaleMainMenu.png")
		saleImg.x = shopBtn.x  + 55
		saleImg.y = shopBtn.y - 18
		mainGroup:insert( saleImg )
		
		local goBack = function()
			transition.to(saleImg, {time = 500, xScale = 1, yScale = 1, y = shopBtn.y - 18, transition = easing.outElastic})
		end
		
		local doEmphasis = function()
			transition.to(saleImg, {time = 500, xScale = 1.1, yScale = 1.1, y = shopBtn.y - 22, transition = easing.inElastic, onComplete = goBack})
		end
		
		doEmphasis()
		timerEmphasis = timer.performWithDelay(3000, doEmphasis, 0)
		timerEmphasis.timerStart = true
	end
	
	noAdsBtn = globals.createImageButton( "noAds",
					display.contentCenterX, shopBtn.y + shopBtn.height + 5,
					"res/graphics/menu/btnRemoveAdsUp.png", "res/graphics/menu/btnRemoveAdsDown.png",
					mainGroup, handleReleaseEvent, 171, 36 )
					
	rectOverlay = display.newRect(display.screenOriginX,display.screenOriginX,3000,3000)
	rectOverlay:setFillColor(0,0,0,0.7)
	pauseGroup:insert( rectOverlay )
	
	local bgWin2 = newImageRectNoDimensions( "res/graphics/game/dialogWin.png" )
	globals.setBetterAnchor( bgWin2 )
	bgWin2.x = display.contentCenterX - bgWin2.width / 2
	bgWin2.y = display.screenOriginY
	pauseGroup:insert( bgWin2 )
	
	local pauseTxt = display.newText("OPTIONS", display.contentCenterX, bgWin2.y + bgWin2.height - 280, font, 28)
	pauseTxt:setFillColor(0,0,0)
	pauseGroup:insert( pauseTxt )
	
	local sfxTxt = display.newText("SOUNDS", bgWin2.x + 40, pauseTxt.y + 50, font, 20)
	globals.setBetterAnchor( sfxTxt )
	sfxTxt:setFillColor(0,0,0)
	pauseGroup:insert( sfxTxt )
	
	local soundBtn
	local musicBtn
	local soundBool = _G.audios.sfx
	local musicBool = _G.audios.music

	-- Temp pls remove
	--soundBool = false
	--musicBool = false
 	
	--> Changes image of buttons in menu
	local function changeState(event)
		local trashGroup = globals.createDisplayGroup( "trashGroup", displayGroups, sceneGroup )
		local trashGroup = globals.getTableMemberWithID( "trashGroup", displayGroups )
		
		--> initializes the button according to saved data
		if soundBtn == nil and musicBtn == nil then
			if soundBool then
				soundBtn = globals.createImageButton( "soundOn",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 280 + 68,
						"res/graphics/menu/btnOnUp.png", "res/graphics/menu/btnOnDown.png",
						pauseGroup, changeState, 81, 31)
				for i = 2,32 do
					audio.setVolume(0.5, {channel=i})
				end	
			else
				soundBtn = globals.createImageButton( "soundOff",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 280 + 68,
						"res/graphics/menu/btnOffUp.png", "res/graphics/menu/btnOffDown.png",
						pauseGroup, changeState, 81, 31 )
				for i = 2,32 do
					audio.setVolume(0, {channel=i})
				end
			end
			
			if musicBool then
				musicBtn = globals.createImageButton( "musicOn",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 160,
						"res/graphics/menu/btnOnUp.png", "res/graphics/menu/btnOnDown.png",
						pauseGroup, changeState, 81, 31 )
				audio.setVolume( 1, { channel=1 } )
			else
				musicBtn = globals.createImageButton( "musicOff",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 160,
						"res/graphics/menu/btnOffUp.png", "res/graphics/menu/btnOffDown.png",
						pauseGroup, changeState, 81, 31 )
				audio.setVolume( 0, { channel=1 } )
			end
		--> changes the state of the buttons
		else
			if(event.target.id == "soundOn") then
				trashGroup:insert(soundBtn)
				soundBtn = nil
				soundBtn = globals.createImageButton( "soundOff",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 280 + 68,
						"res/graphics/menu/btnOffUp.png", "res/graphics/menu/btnOffDown.png",
						pauseGroup, changeState, 81, 31 )
				for i = 2,32 do
					audio.setVolume(0, {channel=i})
				end
				soundBool = false
				settings.set('sfx', soundBool)
			elseif(event.target.id == "musicOn") then
				trashGroup:insert(musicBtn)
				musicBtn = nil
				musicBtn = globals.createImageButton( "musicOff",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 160,
						"res/graphics/menu/btnOffUp.png", "res/graphics/menu/btnOffDown.png",
						pauseGroup, changeState, 81, 31 )
				audio.setVolume( 0, { channel=1 } )
				musicBool = false
				settings.set('music', musicBool)
			elseif(event.target.id == "soundOff") then	
				trashGroup:insert(soundBtn)
				soundBtn = nil
				soundBtn = globals.createImageButton( "soundOn",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 280 + 68,
						"res/graphics/menu/btnOnUp.png", "res/graphics/menu/btnOnDown.png",
						pauseGroup, changeState, 81, 31 )
				for i = 2,32 do
					audio.setVolume(0.5, {channel=i})
				end	
				soundBool = true
				settings.set('sfx', soundBool)
			elseif(event.target.id == "musicOff") then
				trashGroup:insert(musicBtn)
				musicBtn = nil
				musicBtn = globals.createImageButton( "musicOn",
						bgWin2.x + 190, bgWin2.y + bgWin2.height - 160,
						"res/graphics/menu/btnOnUp.png", "res/graphics/menu/btnOnDown.png",
						pauseGroup, changeState, 81, 31 )
				audio.setVolume(1, { channel=1 } )
				musicBool = true
				settings.set('music', musicBool)	
			end
		end
		_G.audios.sfx = soundBool
		_G.audios.music = musicBool
		trashGroup:removeSelf()
	end
	
	changeState()
	
	local sfxTxt2 = display.newText("MUSIC", bgWin2.x + 40, musicBtn.y - 17, font, 20)
	globals.setBetterAnchor( sfxTxt2 )
	sfxTxt2:setFillColor(0,0,0)
	pauseGroup:insert( sfxTxt2 )

	pauseGroup.isVisible = false	

	local closeBtn = globals.createImageButton( "close",
					   bgWin2.x + 230,  bgWin2.y + 110,
						"res/graphics/game/btnCloseUp.png", "res/graphics/game/btnCloseDown.png",
						pauseGroup, closeOptions, 38, 38)
	closeBtn.anchorX = 0 closeBtn.anchorY = 1	
	
	local creditsBtn = globals.createImageButton( "credits",
					display.contentCenterX, bgWin2.y + bgWin2.height - 90,
					"res/graphics/menu/btn_CreditsUp.png", "res/graphics/menu/btn_CreditsDown.png",
					pauseGroup, handleReleaseEvent, 91, 36)
	
	local likeBtn = globals.createImageButton( "likeUs",
					display.contentCenterX, creditsBtn.y + creditsBtn.height + 5,
					"res/graphics/menu/likeUsUp.png", "res/graphics/menu/likeUsDown.png",
					pauseGroup, handleReleaseEvent, 141, 36)

	-- Facebook group popup
	fbRectOverlay = display.newRect(display.screenOriginX,display.screenOriginX,3000,3000)
	fbRectOverlay:setFillColor(0,0,0,0.7)
	fbGroup:insert( fbRectOverlay )
	
	fbGroup.isVisible = false
				
	local bgFBWin = newImageRectNoDimensions( "res/graphics/menu/wordTreatsPopUpBox.png" )
	globals.setBetterAnchor( bgFBWin )
	bgFBWin.x = display.contentCenterX - bgFBWin.width / 2
	bgFBWin.y = display.screenOriginY + bgFBWin.height / 5
	fbGroup:insert( bgFBWin )
	
	local fbCloseBtn = globals.createImageButton( "close",
					   bgFBWin.x + 270,  bgFBWin.y + 50,
						"res/graphics/game/btnCloseUp.png", "res/graphics/game/btnCloseDown.png",
						fbGroup, closeFBPopup, 38, 38)
	fbCloseBtn.anchorX = 0 
	fbCloseBtn.anchorY = 1
	
	local fbGroupBtn = globals.createImageButton( "fb group",
					display.contentCenterX, (bgFBWin.y + bgFBWin.height/1.2) - 20,
					"res/graphics/menu/btnFacebookGroupUp.png", "res/graphics/menu/btnFacebookGroupDown.png",
					fbGroup, handleReleaseEvent, 169, 34)
	
    --Code here runs when the scene is first created but has not yet appeared on screen
	
	local buildText = display.newText(_G.buildNumber, 20, (display.contentHeight -display.screenOriginY) - 20, font, 15)
	--globals.setBetterAnchor( buildText )
	buildText.x = (bgBottom.x - bgBottom.width/2) + 20
	buildText.y = (bgBottom.y + bgBottom.height/2) - 15
	buildText:setFillColor(0,0,0)
	mainGroup:insert(buildText)
	
	--> Detects the last TREAT unlocked
	for i = 1, #_G.mydata do
		if _G.mydata[i].type == 2 and _G.mydata[i].locked == 0 then
			thisMenu = _G.mydata[i].soupName
			treatName = _G.mydata[i].label
		end
	end
	
	for i = 1, #_G.mydata do
		if _G.mydata[i].type == 3 and _G.mydata[i].soupName == thisMenu and _G.mydata[i].locked == 0 then
			thisIngredient = _G.mydata[i].soupIng
			treatIngName = _G.mydata[i].label
			thisIndex = i
			thisLC = _G.mydata[i].lettercount
			thisLevelCount = _G.mydata[i].levelcount
		end
	end
	
	local indexData
	for i = 1, #_G.dataLevels do
		if _G.dataLevels[i].soupName == thisMenu and _G.dataLevels[i].levelName == thisIngredient then
			thisLevel = _G.dataLevels[i].levelNum 
			indexData = i
			if _G.dataLevels[indexData].complete == true and _G.dataLevels[indexData].levelNum + 1 <= thisLevelCount then
				thisLevel = _G.dataLevels[indexData].levelNum + 1
			end
		end
	end
	
	print ("Current Progress: " .. thisMenu .. ": " .. thisIngredient .. "-" .. thisLevel)

	if showFBGroupPopup then
		timer.performWithDelay(1500, openFBPopup)
	else
		showDailyRewardPopup(1500)
	end
end

function scene:updateCoins()
end

function scene:showAds()
end

-- show()
function scene:show( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
        --_G.saleShopBool = true
        _G.isInMenu = true

    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen

    end
end


-- hide()
function scene:hide( event )

    local sceneGroup = self.view
    local phase = event.phase

    if ( phase == "will" ) then
		checkTime()
		if timerEmphasis ~= nil then
			if timerEmphasis.timerStart then
				timer.cancel(timerEmphasis)
				timerEmphasis = nil
			end
		end
        -- Code here runs when the scene is on screen (but is about to go off screen)
    elseif ( phase == "did" ) then
		composer.removeScene( "_mainMenu" )
        -- Code here runs immediately after the scene goes entirely off screen
        scene:showAds()
        _G.isInMenu = false
    end
end


-- destroy()
function scene:destroy( event )

    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
	print("BABYE")

end


-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------

return scene